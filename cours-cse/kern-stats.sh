#!/bin/sh

#
# Génère des graphiques des noyaux Unix/Linux historiques
#
# Usage:
#
#   ./kern-stats.sh init
#	récupère les dépots GIT (volumineux) pour l'exploration
#	de l'historique
#
#   ./kern-stats.sh histdata
#	accumule les données brutes de la taille des noyau
#	dans les fichiers ./kernsize-{bell,bsd,freebsd,linux}.dat
#	Note: nécessite les dépôts GIT comme sous-répertoires.
#
#   ./kern-stats.sh histplot > histsize.pdf
#	génère le graphique de l'historique de la taille des noyaux
#	sur la sortie standard, à partir des données brutes
#	accumulées par "histdata"
#
#   ./kern-stats.sh casmdata > casm.dat
#	accumule les données brutes de la répartition C/assembleur
#	sur la sortie standard
#	Note: nécessite les dépôts GIT comme sous-répertoires.
#
#   ./kern-stats.sh casmplot < casm.dat > casm.pdf
#	génère le graphique de la répartition C/assembleur sur
#	la sortie standard, à partir des données brutes accumulées
#	par "casmdata" (présentées sur l'entrée standard)
#
#   ./kern-stats.sh subdata > subsys.dat
#	accumule les données brutes de la répartition des sous-systèmes
#	de FreeBSD sur la sortie standard
#	Note: nécessite les dépôts GIT comme sous-répertoires.
#
#   ./kern-stats.sh subplot < subsys.dat > subsys.pdf
#	génère le graphique de la répartition des sous-systèmes
#	de FreeBSD sur la sortie standard, à partir des données
#	brutes accumulées par "subdata" (présentées sur l'entrée standard)
#
#   ./kern-stats.sh clean
#	supprime les dépôts GIT créés par "init"
#
# Note : ce script doit être exécuté dans le répertoire contenant
# les dépôts extraits par "init" (sauf pour "init", bien sûr).
#

set -e
set -u

# Format des noms de fichiers générés par "data" et utilisés par "plot"
HISTFMT=kernsize-%s.dat

###############################################################################
# Données de l'historique Unix (Bell - BSD - FreeBSD)

REPO_UNIX="git@github.com:dspinellis/unix-history-repo.git"

#
# Liste des tags ou des branches retenues dans le repository
# Chaque ligne est sous la forme :
#	tag-ou-branche date lignee label répertoire pattern
#
# où :
# -	date est soit "-" (i.e. prendre la date du dernier commit), soit
#	une date corrigée au format "yyyy-mm"
# -	lignee est la lignée pour la légende du graphe
# -	label est l'étiquette pour le graphe
# -	répertoire et pattern sont les arguments de find pour trouver
#	les fichiers du noyau
#

HIST_UNIX="
    Research-PDP7	-	Bell	PDP7	. "s*.s"
    Research-V1 	1971-11	Bell	v1	. "u*.s"
    Research-V4		1973-11	Bell	v4	sys *
    Research-V6		1975-05	Bell	v6	usr/sys *
    Research-V7		1979-01	Bell	v7	usr/sys *
    Bell-32V		1978-06	Bell	32V	usr/src/sys *
    BSD-3		-	BSD	3BSD	usr/src/sys *
    BSD-4		-	BSD	4BSD	usr/src/sys *
    BSD-4_1_snap	-	BSD	4.1BSD	sys *
    BSD-4_2		1983-09	BSD	4.2BSD	usr/src/sys *
    BSD-4_3		1986-06	BSD	4.3BSD	usr/src/sys *
    BSD-4_3_Tahoe	1988-06	BSD	4.3BSD-Tahoe	usr/src/sys *
    BSD-4_3_Reno	1990-06	BSD	4.3BSD-Reno	usr/src/sys *
    BSD-4_3_Net_2	1991-06	BSD	4.3BSD-Net2	usr/src/sys *
    BSD-4_4		1994-06	BSD	4.4BSD	usr/src/sys *
    386BSD-0.0		1992-07	FreeBSD 386BSD	usr/src/sys.386bsd *
    FreeBSD-release/1.0		-	FreeBSD FreeBSD-1 sys *
    "

#    FreeBSD-release/2.0		-	FreeBSD FreeBSD-2 sys *
#    FreeBSD-release/3.0.0	-	FreeBSD FreeBSD-3 sys *
#    FreeBSD-release/4.0.0	-	FreeBSD FreeBSD-4 sys *
#    FreeBSD-release/5.0.0	-	FreeBSD FreeBSD-5 sys *
#    FreeBSD-release/6.0.0	-	FreeBSD FreeBSD-6 sys *
#    FreeBSD-release/7.0.0	-	FreeBSD FreeBSD-7 sys *
#    FreeBSD-release/8.0.0	-	FreeBSD FreeBSD-8 sys *
#    FreeBSD-release/9.0.0	-	FreeBSD FreeBSD-9 sys *
#    FreeBSD-release/10.0.0	-	FreeBSD FreeBSD-10 sys *
#    FreeBSD-release/11.0.0	-	FreeBSD FreeBSD-11 sys *

#
# Liste des tags non pris car trop proches d'un tag existant (pour
# la représentation graphique) ou ne contenant pas des données assez
# fiables
#

PAS_PRIS="
    Research-V5 usr/sys *
    BSD-4_1c_2  a/sys *
    BSD-4_4_Lite1 usr/src/sys *
    BSD-4_4_Lite2 usr/src/sys *
    BSD-SCCS-END usr/src/sys *
    386BSD-0.1 usr/src/sys.386bsd *
    FreeBSD-release/1.1 sys *
    FreeBSD-release/1.1.5 sys *
"

###############################################################################
# Données de FreeBSD récents

REPO_FBSD="https://github.com/freebsd/freebsd-src"

# Dernière version de FreeBSD <tag-or-branch> <label> <annee>
LAST_FBSD="release/13.1.0	FreeBSD_13.1	2022"

HIST_FBSD="
    release/2.0		-	FreeBSD FreeBSD-2 sys *
    release/3.0.0	-	FreeBSD FreeBSD-3 sys *
    release/4.0.0	-	FreeBSD FreeBSD-4 sys *
    release/5.0.0	-	FreeBSD FreeBSD-5 sys *
    release/6.0.0	-	FreeBSD FreeBSD-6 sys *
    release/7.0.0	-	FreeBSD FreeBSD-7 sys *
    release/8.0.0	-	FreeBSD FreeBSD-8 sys *
    release/9.0.0	-	FreeBSD FreeBSD-9 sys *
    release/10.0.0	-	FreeBSD FreeBSD-10 sys *
    release/11.0.0	-	FreeBSD FreeBSD-11 sys *
    release/12.0.0	-	FreeBSD FreeBSD-12 sys *
    release/13.0.0	-	FreeBSD FreeBSD-13 sys *
"

###############################################################################
# Données de Linux

REPO_LINUX="https://github.com/torvalds/linux.git"

HIST_LINUX="
    v2.6.12	- Linux 2.6.12 . *.[chylsS]
    v2.6.20	- Linux 2.6.20 . *.[chylsS]
    v2.6.30	- Linux 2.6.30 . *.[chylsS]
    v3.0	- Linux 3.0 . *.[chylsS]
    v3.10	- Linux 3.10 . *.[chylsS]
    v4.0	- Linux 4.0 . *.[chylsS]
    v4.10	- Linux 4.10 . *.[chylsS]
    v4.17	- Linux 4.17 . *.[chylsS]
    v5.8	- Linux 5.8 . *.[chylsS]
    v5.14	- Linux 5.14 . *.[chylsS]
    v6.0-rc4	- Linux 6.0-rc4 . *.[chylsS]
"

###############################################################################
# Données de Linux historique

REPO_OLDLINUX="https://git.kernel.org/pub/scm/linux/kernel/git/history/history.git"

HIST_OLDLINUX="
    0.10	1991-12	Linux - . *.[chylsS]
    0.95	1992-03	Linux - . *.[chylsS]
    0.99	1992-12	Linux - . *.[chylsS]
    0.99.14	1993-11 Linux - . *.[chylsS]
    1.0		1994-03 Linux - . *.[chylsS]
    1.2.0	1995-03 Linux - . *.[chylsS]
    2.0		1996-06 Linux - . *.[chylsS]
    2.1.0	1996-09 Linux 2.1 . *.[chylsS]
    2.2.0	1999-01 Linux 2.2 . *.[chylsS]
    2.4.0-test13pre7 	2001-01 Linux 2.4 . *.[chylsS]
    v2.5.0	2001-11 Linux 2.5 . *.[chylsS]
"

###############################################################################
# Données pour la répartition C / Assembleur
#
# Chaque ligne est sous la forme :
#	repo tag-ou-branche label annee repertoire
# où :
# -	repo est le dépot (https://..etc)
# -	tag-or-branch est le nom de la version dans le repo
# -	label est l'étiquette pour le graphe
# -	annee est l'année de cette version
# -	repertoire est le répertoire à analyser
# Note : pour FreeBSD, les données sont issues de la variable LAST_FBSD
#
CASM="
	$REPO_UNIX	Research-V6	Bell_v6		1975	usr/sys
	$REPO_FBSD	$LAST_FBSD				sys
	$REPO_LINUX	v6.0-rc4	Linux_6.0-rc4	2022	.
"

###############################################################################
# Les fonctions utiles
###############################################################################

usage ()
{
    (
	echo "usage: $0 init|histdata|histplot|casmdata|casmplot|subdata|subplot|clean ..."
	echo "	init : clone les dépôts git"
	echo "	histdata : génère les données de taille en fct de l'historique"
	echo "	histplot : génère le graphe"
	echo "	casmdata : génère les données de taille en fct du langage"
	echo "	casmplot : génère le graphe"
	echo "	subdata : génère les données de taille des sous-systèmes"
	echo "	subplot : génère le graphe"
	echo "	clean : supprime les dépôts git"
    ) >&2
    exit 1
}

repo2dir ()
{
    local repo="$1"

    echo "$repo" | sed -e 's/\.git$//' -e 's:.*/::'
}

# détermine le répertoire à partir du dépôt, vérifie qu'il
# est présent, et renvoie ce répertoire
check_repodir ()
{
    local repo="$1"
    local repodir=$(repo2dir "$repo")
    if [ ! -d "$repodir" ]
    then
	echo "Répertoire '$repodir' non trouvé" >&2
	echo "Faire: git clone -quiet '$repo'" >&2
	exit 1
    fi
    echo "$repodir"
}

# calcule le nombre de lignes contenues dans les fichiers dont les
# noms sont présentés sur l'entrée standard
nlines()
{
    xargs cat | wc -l
}

###############################################################################
# Initialisation et suppression des dépôts git
###############################################################################

init ()
{
    local repo="$1"

    git clone --quiet "$repo"
}

clean ()
{
    local repo="$1"
    local dir=$(repo2dir "$repo")

    if [ -d "$dir" ]
    then rm -rf "$dir"
    fi
}

###############################################################################
# Calcul des données de l'historique
###############################################################################

commit_date ()
{
    git log --max-count=1 --date=short | sed -n 's/^Date: //p'
}

line_count ()
{
    local dir="$1" pattern="$2"

    find "$dir/" -type f -name "$pattern" | nlines
}

# renvoie le fichier associé à la lignée (kernsize-xxxx.dat)
lineage2file ()
{
    local lignee="$1"

    printf "$HISTFMT\n" $(echo $lignee | tr A-Z a-z)
}

# supprime le fichier correspondant aux lignées indiquées
reset_lineage ()
{
    local lignees="$1"
    local l

    for l in $lignees
    do
	rm -f $(lineage2file $l)
    done
}

histdata ()
{
    local repo="$1" liste="$2"
    local repodir

    repodir=$(check_repodir "$repo")

    (
	cd "$repodir"
	echo "$liste" | \
	    while read tagorbranch date lineage label sysdir pattern
	    do
		if [ ! -z "$tagorbranch" ]	# lignes vides dans la variable
		then
		    git checkout --quiet "$tagorbranch"

		    if [ "x$date" = "x-" ]
		    then date=$(commit_date)
		    else date="$date-01"	# pour avoir un numéro de jour
		    fi

		    if [ "x$label" = "x-" ]
		    then label=$(echo $tagorbranch | sed 's/_/-/g')
		    fi

		    n=$(line_count "$sysdir" "$pattern")

		    f=$(lineage2file $lineage)
		    echo $date $label $n >> "../$f"
		fi
	    done
    )
}

sort_lineage ()
{
    local lignees="$1"
    local l f

    for l in $lignees
    do
	f=$(lineage2file $l)
	sort < $f > $f.bak && mv $f.bak $f
    done
}

###############################################################################
# Graphique de l'historique
###############################################################################

histplot ()
{
    local lignees="$1"
    local l f first comma

    for l in $lignees
    do
	f=$(lineage2file $l)
	if [ ! -f "$f" ]
	then
	    echo "Fichier '$f' non trouvé. Utiliser la commande 'data'">&2
	    exit 1
	fi
    done

    cat <<'EOF'
	# set xlabel "Date"
	set xdata time
	set timefmt "%Y-%m-%d"
	set format x "%Y"
	# set format y "%h"		# 10000 ou 10^5
	# set format y "%.0sx10^{%S}"	# 100 x 10^3
	# set format y " 10^{%T}"	# 10^5
	set format y "%.0s %c"		# 100 K
	set logscale y
	set grid
	set key left
	set term pdf
	set title "Taille des sources du noyau sur plusieurs systèmes"
	set ylabel "Nombre de lignes"
	plot \
EOF

    # plotter le nb de lignes
    first=true
    for l in $lignees
    do
	f=$(lineage2file $l)
	if [ $first = true ]
	then comma=""
	else comma=","
	fi
	echo "$comma '$f' using 1:3 with linespoints title '$l' \\"
	first=false
    done

    # plotter les noms des releases
    #for l in $lignees
    #do
    #    f=$(lineage2file $l)
    #    echo ", '$f' using 1:(\$3*1.3):2:(90) with labels rotate variable title '' \\"
    #done
}

###############################################################################
# Calcul de la répartition C/Asm/Autres
###############################################################################

sourceC ()
{
    local sysdir="$1"
    (
	find $sysdir -type f -name "*.[ch]"
	find $sysdir -type f -name "[mM]akefile"
    ) | nlines
}

asm ()
{
    local sysdir="$1"
    find $sysdir -type f -name "*.[sS]" | nlines
}

autres ()
{
    local sysdir="$1"
    find $sysdir -type f \
	| grep -v '\.[sShc]$'\
	| grep -v '.dtsi*$' \
	| grep -v '.rst$' \
	| grep -v '.ppm$' \
	| grep -v '.txt$' \
	| grep -v '.yaml$' \
	| grep -v '.json$' \
	| grep -v Makefile\
	| nlines
}

total ()
{
    local sysdir="$1"
    find $sysdir -type f | nlines
}

casmdata ()
{
    echo 'Version Assembleur "Source C" Autres'
    echo "$CASM" | \
	while read repo tagorbranch label annee sysdir
	do
	    if [ ! -z "$repo" ]		# lignes vides dans la variable
	    then
		(
		    repodir=$(check_repodir "$repo")
		    cd $repodir
		    git checkout --quiet "$tagorbranch"
		    nasm=$(asm "$sysdir")
		    nc=$(sourceC "$sysdir")
		    nautres=$(autres "$sysdir")
		    ntotal=$(total $sysdir)
		    label=$(echo $label | sed 's/_/ /g')
		    echo "\"$label ($annee)\" $nasm $nc $nautres"
		)
	    fi
	done
}


casmplot ()
{
    cat <<'EOF'
	set style data histogram
	set style histogram cluster gap 2
	set style fill solid noborder
	set boxwidth 0.9 relative
	set grid ytics # mytics
	set logscale y
	set format x ''
	unset xtics
	set xtics auto
	set xtics scale 0
	set xtics font ",10"
	set format y "%.0s %c"
	set key autotitle columnheader
	set key left
	set term pdf
	set title "Répartition du nombre de lignes par langage"
	set ylabel "Nombre de lignes"
	# accumuler les données présentées sur l'entrée std pour
	# pouvoir les utiliser plusieurs fois avec plot
	$casm << EOD
EOF
    cat
    echo "EOD"
    echo 'plot $casm using 2:xtic(1), $casm using 3, $casm using 4'
}

###############################################################################
# Taille des sous-systèmes sur FreeBSD
###############################################################################

subdata ()
{
    local repo="$1"
    # analyse de LAST_FBSD passé sous forme de 3 arguments
    local tagorbranch="$2" label="$3" annee="$4"

    local repodir=$(check_repodir "$repo")
    local liste="
	Cœur	sys/kern sys/libkern sys/sys sys/xen
	Mém_virt	sys/vm
	Sécurité	sys/bsm sys/crypto sys/kgssapi sys/netpfil sys/opencrypto sys/security
	Syst_fich	sys/cddl sys/fs sys/geom sys/netsmb sys/nfs sys/nfsclient sys/nfsserver sys/nlm
	Réseau	sys/net sys/net80211 sys/netgraph sys/netinet sys/netinet6 sys/netipsec sys/ofed sys/rpc sys/xdr
	Périph	sys/cam sys/dev sys/isa sys/modules
	Arch	sys/amd64 sys/arm sys/arm64 sys/i386 sys/mips sys/powerpc sys/riscv sys/x86
	Dével	sys/ddb sys/gdb sys/tests sys/tools sys/conf
    "
    local titre lrep

    (
	cd $repodir
	git checkout --quiet "$tagorbranch"

	# Titres des colonnes

	# Les valeurs
	echo "$liste" | \
	    while read titre lrep
	    do
		if [ ! -z "$titre" ]	# lignes vides dans la variable
		then
		    titre=$(echo "\"$titre\"" | sed -e 's/_/ /g')
		    n=$(find $lrep -type f -print | nlines)
		    echo "$titre $n"
		fi
	    done
    )
}


subplot ()
{
    # analyse de LAST_FBSD passé sous forme de 3 arguments
    local tagorbranch="$1" label="$2" annee="$3"
    local version="$(echo "$label ($annee)" | sed 's/_/ /g')"

    sed "s/%VERSION%/$version/" <<'EOF'
	set style data histograms
	# set style histogram cluster gap 2
	set style fill solid noborder
	set boxwidth 1.8 relative
	set grid ytics # mytics
	set logscale y
	# set xtics auto
	set xtics scale 0
	# set xtics rotate by -15
	set xtics offset -0.3,0 center font ",10"
	set format y "%.0s %c"
	set nokey
	set term pdf
	set title "Répartition des sous-systèmes sur %VERSION%"
	set ylabel "Nombre de lignes"
	# accumuler les données présentées sur l'entrée std pour
	# pouvoir les utiliser plusieurs fois avec plot
	$subsys << EOD
EOF
    cat
    echo "EOD"
    echo 'plot $subsys using 2:xtic(1)'
}

###############################################################################
# Programme principal
###############################################################################

if [ $# = 0 ]
then usage $0
fi

for arg
do
    case "$arg" in
	init)
	    init "$REPO_UNIX"
	    init "$REPO_FBSD"
	    init "$REPO_OLDLINUX"
	    init "$REPO_LINUX"
	    ;;

	histdata)
	    reset_lineage "Bell BSD FreeBSD Linux"

	    # génère Bell, BSD et début de FreeBSD
	    histdata "$REPO_UNIX" "$HIST_UNIX"

	    # génère la suite de FreeBSD
	    histdata "$REPO_FBSD" "$HIST_FBSD"

	    # génère (en deux fois) Linux
	    histdata "$REPO_OLDLINUX" "$HIST_OLDLINUX"
	    histdata "$REPO_LINUX" "$HIST_LINUX"

	    sort_lineage "Bell BSD FreeBSD Linux"
	    ;;
	histplot)
	    histplot "Bell BSD FreeBSD Linux" | gnuplot
	    ;;

	casmdata)
	    casmdata
	    ;;
	casmplot)
	    casmplot | gnuplot
	    ;;

	subdata)
	    subdata "$REPO_FBSD" $LAST_FBSD	# 4 args en tout
	    ;;
	subplot)
	    subplot $LAST_FBSD | gnuplot
	    ;;

	clean)
	    clean "$REPO_UNIX"
	    clean "$REPO_FBSD"
	    clean "$REPO_OLDLINUX"
	    clean "$REPO_LINUX"
	    ;;
	*)
	    usage $0
	    ;;
    esac
done
