\documentclass [landscape,a4paper] {article}

    \usepackage [french] {babel}
    \frenchsetup{AutoSpacePunctuation=false}
    \usepackage [utf8] {inputenc}
    \usepackage [T1] {fontenc}

    % \raggedbottom

    \pagestyle{empty}
    \usepackage [margin=10mm] {geometry}
    \usepackage {array}
    \frenchspacing
    \parskip=2mm
    \parindent=0mm
    \renewcommand{\baselinestretch}{0.9}

    \usepackage [scaled] {helvet}
    \usepackage {courier}

    \usepackage {listings}
    \lstset {
	language=c,
	basicstyle=\footnotesize\ttfamily,
	texcl=true,
	commentstyle=\sffamily\itshape,
	keywordstyle=
    }

    \usepackage {graphicx}
    \setkeys {Gin} {keepaspectratio}

    \usepackage {multicol}
    \raggedcolumns
    % \setlength\columnsep{20pt}
    % \setlength\columnseprule{.4pt}

    \newcommand {\piedpage} {%
	\vfill
	\includegraphics [width=.1\textwidth] {logo-uds}
	\hfill
	\footnotesize
	\input{annee}
	\hfill
	Pierre David, pda@unistra.fr
    }

    \newcommand {\titreA} [1] { %
	\setlength\fboxsep{1.5mm} %
	\framebox [\linewidth] {\centering \huge\textbf{#1}} \par
    }

    \newcommand {\titreB} [1] { %
	\setlength\fboxsep{1.5mm} %
	\framebox [\linewidth] {%
	    \centering \large\textbf{#1} \rule[-2pt]{0pt}{0pt} %
	}
	\par
    }

    \newcommand {\titreC} [1] { %
	\setlength\fboxsep{1mm} %
	\framebox [\linewidth] {\centering \textbf{#1}} \par
    }


    % \syntaxe {instr} {modesrc} {modedst}
    % ex : \syntaxe {st} {reg} {dir/ind/idx}
    \newcommand {\syntaxedeux} [3] {%
	\texttt{#1 }\emph{#2}\texttt{,}\emph{#3} %
    }
    \newcommand {\syntaxeun} [2] {%
	\texttt{#1 }\emph{#2} %
    }

    % \caseinstr {syntaxe-a-gauche} {exemple-a-droite} {desc en dessous}
    \newcommand{\caseinstr} [3] {%
	\makebox [0.5\linewidth] [l] {#1}
	\hfill
	\makebox [0.4\linewidth] [r] {\texttt{#2}}
	\\
	\hspace* {0.08\linewidth} \parbox [t] {.92\linewidth} {#3} %
    }

    % \reg {\%a} {registre général}
    \newcommand{\reg}[2] { %
	\makebox [.1\linewidth] [l] {\small#1} 
	    \parbox [t] {.9\linewidth} {\small#2}
    }

    \newcommand{\bitx}[3] { %
	{\small
	\makebox [.05\linewidth] [l] {#1}
	    \makebox [.25\linewidth] [l] {#2}
	    \makebox [.70\linewidth] [l] {#3}
	}
    }

    % \bitsr {S} {superviseur} {1 si mode superviseur}
    \newcommand{\bitsr}[3] {\bitx {#1} {(\emph{#2\/})} {#3}}

    % \ioreg {registre d'état (R)} {tableau}
    \newcommand{\ioreg} [2] {%
	\makebox [0.3\linewidth] [l] {#1}
	\hfill
	\makebox [0.6\linewidth] [r] { %
	    \renewcommand{\arraystretch}{1.1}
	    \setlength\extrarowheight{2pt}
	    \footnotesize
	    #2
	}
    }

\begin {document}
\sffamily
\small

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\titreA {\huge\textbf{Zorglub33 -- Aide mémoire}}

\begin {multicols} {3}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Registres}

    \reg{A}{registre général}
    \reg{B}{registre général}
    \reg{PC}{compteur ordinal / \emph{program counter}}
    \reg{SP}{pointeur de pile / \emph{stack pointer}}
    \reg{SR}{registre d'état / \emph{status register}}

\vspace* {-4mm}

Le registre d'état comporte les bits suivants~:

\centerline {
\footnotesize
\setlength\extrarowheight{2pt}
\renewcommand{\arraystretch}{1.1}
\begin {tabular} {r|*{9}{c|}}
    \multicolumn {1} {c} {} &
    \multicolumn {1} {c} {...} &
    \multicolumn {1} {c} {9} &
    \multicolumn {1} {c} {8} &
    \multicolumn {1} {c} {...} &
    \multicolumn {1} {c} {3} &
    \multicolumn {1} {c} {2} &
    \multicolumn {1} {c} {1} &
    \multicolumn {1} {c} {0} \\ \cline{2-9}
    SR & ... & S & IE & ... & O & N & Z & C
    \\ \cline{2-9}
\end {tabular}
}

    Bits S et IE : accessibles en mode superviseur \\
    \bitsr{S}{supervisor}{1 : mode superviseur, 0 : mode utilisateur}
    \bitsr{IE}{interrupt enable}{1 : interruptions activées,
	0 : masquées}

    \vspace* {-4mm}

    Bits O, N, Z, C : accessibles en mode utilisateur ou superviseur \\
    Valeur = fonction de la dernière opération \texttt{cmp} ou arithmétique
    \\
    \bitsr{O}{overflow}{dépassement de capacité}
    \bitsr{N}{negative}{résultat négatif}
    \bitsr{Z}{zero}{résultat nul}
    \bitsr{C}{carry}{l'opération a provoqué une retenue}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB{Interruptions et exceptions}

En cas d'interruption ou d'exception, le processeur~:
\begin {itemize}
    \item sauvegarde le registre PC à l'adresse 100
    \item sauvegarde le registre SR à l'adresse 101
    \item sauvegarde le code de l'événement à l'adresse 102
    \item met le bit SR:S à 1
    \item met le bit SR:IE à 0 s'il s'agit d'une interruption
    \item met la valeur 200 dans PC
\end {itemize}

\vspace* {1mm}

Codes d'événements~:

\vspace*{1mm}

\centerline {
\footnotesize
\renewcommand{\arraystretch}{1.1}
\setlength\extrarowheight{1pt}
\begin {tabular} {|r|l|} \hline
    0 & interruption matérielle \\ \hline
    1 & division par zéro \\ \hline
    2 & tentative d'exécution d'une instruction invalide \\ \hline
    3 & tentative d'exécution d'une instruction privilégiée \\ \hline
    4 & exécution de l'instruction \texttt{trap} \\ \hline
    5 & accès mémoire invalide \\ \hline
\end {tabular}
}

\vspace*{1mm}

Lorsque le processeur exécute l'instruction \texttt{rti}, il~:
\begin {itemize}
    \item restaure le registre PC à partir de l'adresse 100
    \item restaure le registre SR à partir de l'adresse 101
\end {itemize}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\columnbreak

\titreB {Contrôleur de clavier}

    \vspace* {-3mm}

    \ioreg {Registre d'état (R/-)} {
	\begin {tabular} {*{5}{c|}}
	    \multicolumn {1} {c} {} &
	    \multicolumn {1} {c} {...} &
	    \multicolumn {1} {c} {2} &
	    \multicolumn {1} {c} {1} &
	    \multicolumn {1} {c} {0} \\ \cline{2-5}
	    état (20) & ... & - & I & R
	    \\ \cline{2-5}
	\end {tabular}
    }

    \bitsr{I}{interrupt}{1 : le contrôleur a généré une interruption}
    \bitsr{R}{ready}{1 : donnée prête à être lue}
    La lecture du registre d'état provoque la remise à 0 du bit I.

    \vspace* {-2mm}

    \ioreg {Registre de contrôle (-/W)} {
	\begin {tabular} {*{4}{c|}}
	    \multicolumn {1} {c} {} &
	    \multicolumn {1} {c} {...} &
	    \multicolumn {1} {c} {1} &
	    \multicolumn {1} {c} {0} \\ \cline{2-4}
	    contrôle (20) & ... & I & L 
	    \\ \cline{2-4}
	\end {tabular}
    }

    \bitsr{I}{interrupt}{1 : génère une interrupt. si donnée disponible}
    \bitsr{L}{LED}{1 : allumer ou éteindre une LED du clavier}

    \vspace* {-3mm}

    \ioreg {Registre de donnée (R/-)} {
	\begin {tabular} {*{6}{c|}}
	    \multicolumn {1} {c} {} &
	    \multicolumn {1} {c} {...} &
	    \multicolumn {1} {c} {9} &
	    \multicolumn {1} {c} {8} &
	    \multicolumn {1} {c} {7 ... 4} &
	    \multicolumn {1} {c} {3 ... 0} \\ \cline{2-6}
	    donnée (21) & ... & C & S & col & lig
	    \\ \cline{2-6}
	\end {tabular}
    }

    \bitsr{C}{control}{1 : touche Ctrl enfoncée}
    \bitsr{S}{shift}{1 : touche Shift enfoncée}
    \bitsr{col}{colonne}{numéro de colonne de la touche enfoncée}
    \bitsr{lig}{ligne}{numéro de ligne de la touche enfoncée}

    \vspace* {-3mm}

    \ioreg {Registre de donnée (-/W)} {
	\begin {tabular} {*{4}{c|}}
	    \multicolumn {1} {c} {} &
	    \multicolumn {1} {c} {...} &
	    \multicolumn {1} {c} {3} &
	    \multicolumn {1} {c} {2 ... 0} \\ \cline{2-4}
	    donnée (21) & ... & S & led
	    \\ \cline{2-4}
	\end {tabular}
    }

    \bitsr{S}{switch}{1 : allumer la LED, 0 : éteindre}
    \bitsr{led}{numéro}{numéro de la LED à allumer ou éteindre}

    \vspace* {-2mm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Contrôleur de disque}

    \vspace* {-3mm}

    \ioreg {Registre d'état (R/-)} {
	\begin {tabular} {*{6}{c|}}
	    \multicolumn {1} {c} {} &
	    \multicolumn {1} {c} {...} &
	    \multicolumn {1} {c} {3} &
	    \multicolumn {1} {c} {2} &
	    \multicolumn {1} {c} {1} &
	    \multicolumn {1} {c} {0} \\ \cline{2-6}
	    état (50) & ... & E & I & R & A
	    \\ \cline{2-6}
	\end {tabular}
    }

    \bitsr{E}{error}{1 : une erreur a été détectée}
    \bitsr{I}{interrupt}{1 : le contrôleur a généré une interruption}
    \bitsr{R}{ready}{1 : donnée prête à être lue}
    \bitsr{A}{available}{1 : disque inactif}
    La lecture du registre d'état provoque la remise à 0 des bits I et E.

    \vspace* {-2mm}

    \ioreg {Registre de contrôle (-/W)} {
	\begin {tabular} {*{6}{c|}}
	    \multicolumn {1} {c} {} &
	    \multicolumn {1} {c} {...} &
	    \multicolumn {1} {c} {3} &
	    \multicolumn {1} {c} {2} &
	    \multicolumn {1} {c} {1} &
	    \multicolumn {1} {c} {0} \\ \cline{2-6}
	    contrôle (50) & ... & I & L & R & W
	    \\ \cline{2-6}
	\end {tabular}
    }

    \bitsr{I}{interrupt}{1 : génère une interruption après transfert}
    \bitsr{L}{location}{1 : registres de données contiennent C/H/S}
    \bitsr{R}{read}{1 : lance une requête de lecture}
    \bitsr{W}{write}{1 : lance une requête d'écriture}

    \vspace* {-4mm}

    \ioreg {Registres de données (R/W)} {
	\begin {tabular} {*{6}{c|}}
	    \multicolumn {1} {c} {} &
	    \multicolumn {1} {c} {7 ~ ~ ~ ~ ... ~ ~ ~ ~ 0} \\ \cline{2-2}
	    données (51 à 100) & octet
	    \\ \cline{2-2}
	\end {tabular}
    }

    \bitx{octet}{\hspace* {5mm}emplacement (C/H/S), ou données lues ou à écrire}{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\columnbreak

\titreB{Zorglub33-M}

Par rapport au Zorglub33, le Zorglub33-M dispose de deux registres LIM
et BASE supplémentaires~:

\begin {itemize}
    \item mode superviseur~: registres non utilisés
    \item mode utilisateur~: accès mémoire à une adresse $\lambda$~:
	\begin {itemize}
	    \item si $\lambda \in [0,$\texttt{\%lim}$[$, alors
		\begin {itemize}
		    \item convertir l'adresse : $\varphi = \lambda + $\texttt{\%base}
		\end {itemize}
	    \item sinon générer l'exception «~accès mémoire invalide~»
	\end {itemize}

\end {itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB{Zorglub33-S}

Par rapport au Zorglub33, le Zorglub33-S ajoute 10 couples de registres
<LIM$_s$, BASE$_s$> ($0 \leq s < 10$)~:

\begin {itemize}
    \item mode superviseur~: registres non utilisés
    \item mode utilisateur~: accès mémoire à une adresse $\lambda$~:
	\begin {itemize}
	    \item segment $s = \lfloor\frac{\lambda}{1000}\rfloor$,
		offset $o = \lambda \mbox{ mod } 1000$
	    \item si $o \in [0,$\texttt{\%lim}$_s[$, alors
		\begin {itemize}
		    \item convertir l'adresse : $\varphi = \lambda + $\texttt{\%base}$_s$
		\end {itemize}
	    \item sinon générer l'exception «~accès mémoire invalide~»
	\end {itemize}

\end {itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB{Zorglub33-V}

Par rapport au Zorglub33, le Zorglub33-V ajoute un registre PT (\emph{page
table\/}) indiquant l'adresse de la table des pages, utilisée pour tout accès
(en mode superviseur ou utilisateur)~:

Chaque entrée de la table des pages a le format suivant~:

\centerline {
\setlength\extrarowheight{2pt}
\renewcommand{\arraystretch}{1.1}
\footnotesize
\begin {tabular} {|*{14}{c|}}
    \multicolumn {1} {c} {...} &
    \multicolumn {1} {c} {11} &
    \multicolumn {1} {c} {10} &
    \multicolumn {1} {c} {9} &
    \multicolumn {1} {c} {8} &
    \multicolumn {1} {c} {8} &
    \multicolumn {1} {c} {7} &
    \multicolumn {1} {c} {6 ~ ~ ~ ... ~ ~ ~ 0} \\ \hline
    ... & P & R & X & S & A & D & adrcadre \\ \hline
\end {tabular}
}

Les flags de chaque entrée sont~:

    \bitsr{P}{present}{1 : page présente, 0 : page absente}
    \bitsr{R}{read-only}{1 : lecture seule, 0 : lecture/écriture}
    \bitsr{X}{execute}{1 : exécution possible, 0 : pas d'exécution}
    \bitsr{S}{supervisor}{1 : accès en mode superviseur seulement}
    \bitsr{A}{accessed}{1 : page accédée, 0 : page non accédée}
    \bitsr{D}{dirty}{1 : page modifiée : 0 : page non modifiée}

\vspace* {-4mm}

Lors d'un accès mémoire (en mode U ou S) à l'adresse $\lambda$~:
\begin {itemize}
    \item numéro de page $p = \lfloor\frac{\lambda}{100}\rfloor$,
	offset $o = \lambda \mbox {~mod~} 100$
    \item lecture de la case mémoire d'adresse \texttt{\%pt}$+p$
    \item si flags \{P, R, X, S\} ok, alors
	\begin {itemize}
	    \item convertir l'adresse : $\varphi = \mbox{adrcadre} \times 100 + o$
	    \item actualiser les flags  \{A, D\} si nécessaire
	\end {itemize}
    \item sinon générer l'exception «~accès mémoire invalide~»
\end {itemize}

\end {multicols}

\piedpage

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage

\titreA {\huge\textbf{Zorglub33 -- Aide mémoire}}

\begin {multicols} {3}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB{Modes d'adressage}

% \modeadr {imm} {Immédiat} {valeur indiquée} {ld 100,\%a}
\newcommand{\modeadr}[4] { %
    \parskip=1mm
    \makebox [.08\linewidth] [l] {\small\emph{#1}} 
    \parbox [t] {.92\linewidth} { %
	\small %
	\makebox[\linewidth]{#2 \hfill \makebox [10em][l] {\texttt{#4}}}
	#3
    }
    \par
}

    \modeadr{imm} {Immédiat} {valeur indiquée} {ld 100,\%a}
    \modeadr{reg} {Registre} {contenu du registre} {ld \%b,\%a}
    \modeadr{dir} {Direct} {adresse fournie} {ld [100],\%a}
    \modeadr{ind} {Indirect} {adresse pointée par le registre} {ld [\%b],\%a}
    \modeadr{idx} {Indirect indexé} {adresse pointée par registre +/- déplacement} {ld [\%b-5],\%a}


\titreC {Exemple : instruction \texttt{ld}}

    Syntaxe : \syntaxedeux{ld}{imm/reg/dir/ind/idx} {reg}

    \newcommand{\exmode}[3] { %
	\makebox [.27\linewidth] [l] {\small\texttt{#1}} 
	\parbox [t] {.73\linewidth} {\small#2 $\leftarrow$ #3} \\
    }
    \exmode {ld 100,\%a} {a} {valeur 100}
    \exmode {ld \%b,\%a} {a} {contenu du registre b}
    \exmode {ld [100],\%a} {a} {contenu de la mémoire à l'adresse 100}
    \exmode {ld [\%b],\%a} {a} {c. de la mém à l'adresse indiquée par b}
    \exmode {ld [\%b+3],\%a} {a} {c. de la mém à l'adresse indiquée par b+3}

\vspace* {-2mm}

\titreC {Exemple : instruction \texttt{st}}

    Syntaxe : \syntaxedeux{st}{reg}{dir/ind/idx}

    \exmode {st \%a,[100]} {mémoire à l'adresse 100} {a}
    \exmode {st \%a,[\%b]} {mémoire à l'adresse indiquée par b} {a}
    \exmode {st \%a,[\%b-5]} {mémoire à l'adresse indiquée par b-5} {a}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB{Préprocesseur}

% Le préprocesseur du langage C est utilisable~:

    \caseinstr {\texttt{\#define} \emph{symbole} \emph{val}}
	{\#define MAX 0x50}
	{définit le symbole à la valeur indiquée}
    \caseinstr {\texttt{\#include <} \emph{fichier} \texttt{>}}
	{\#include <defs.h>}
	{inclut les définitions du fichier}
    \caseinstr {\texttt{\#if} \emph{expression}
		/ \texttt{\#else}
		/ \texttt{\#endif}}
	{\#if defined(MAX)}
	{traitement conditionnel des instructions suivantes}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB{Directives assembleur}

    \caseinstr {\emph{symbole}\texttt{:}}
	{debut:}
	{associe l'adresse actuelle au symbole}
    \caseinstr {\texttt{.addr }\emph{val}}
	{.addr 200}
	{modifie l'adresse actuelle}
    \caseinstr {\texttt{.space }\emph{val}}
	{.space 4}
	{réserve \emph{val} octets à l'adresse actuelle}
    \caseinstr {\texttt{.word }\emph{val}}
	{.word 0x1000}
	{réserve un mot à l'adresse actuelle et y place \emph{val}}
    \caseinstr {\texttt{.string }\emph{chaîne}}
	{.string  "hello"}
	{place la chaîne (avec un octet nul) à l'adresse actuelle}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\columnbreak

\titreB{Instructions}

\newcommand{\instrdeux}[5] {%
    \caseinstr {\syntaxedeux{#1}{#2}{#3}} {#4} {#5}
}
\newcommand{\instrun}[4] {%
    \caseinstr {\syntaxeun{#1}{#2}} {#3} {#4}
}

    \instrdeux {add} {imm/reg/dir/ind/idx} {reg}
	{add 5,\%a}
	{ajoute une valeur à un registre}
    \instrdeux {and} {imm/reg/dir/ind/idx} {reg}
	{and 0xf8,\%a}
	{\emph{et} bit à bit avec la valeur indiquée}
    \instrun {call} {imm/reg/dir/ind/idx}
	{call 8399}
	{empiler \texttt{\%pc} et aller à l'adresse indiquée}
    \instrdeux {cmp} {imm/reg/dir/ind/idx} {reg}
	{cmp 0,\%a}
	{compare une valeur à un registre (résultat = bits de SR)}
    \instrdeux {div} {imm/reg/dir/ind/idx} {reg}
	{div [100],\%a}
	{divise un registre par une valeur}
    \instrdeux {fas} {dir/ind/idx} {reg}
	{fas [2500],\%a}
	{charge le contenu d'une case mémoire dans un registre et met cette case à 1}
	\\[0.05pt]
    \instrdeux {in} {dir/ind/idx} {reg}
	{in [50],\%b}
	{lit une valeur depuis un contrôleur d'entrée/sortie}
    \instrun {jmp} {imm/reg/dir/ind/idx}
	{jmp \%b}
	{saut inconditionnel}
    \instrun {j\emph{xx}} {imm/reg/dir/ind/idx}
	{jeq 3700}
	{saut conditionnel, avec \texttt{\emph{xx}} en fonction des bits de SR : \\
	    \hspace*{.02\linewidth}\parbox [t] {.9\linewidth} { %
		\texttt{jeq} : saut si égal \\
		\texttt{jne} : saut si différent \\
		\texttt{jle} : saut si inférieur ou égal \\
		\texttt{jlt} : saut si inférieur strict \\
		\texttt{jge} : saut si supérieur ou égal \\
		\texttt{jgt} : saut si supérieur strict
	    }
	    \vspace* {2pt}
	}
    \instrdeux{ld} {imm/reg/dir/ind/idx} {reg}
	{ld [100],\%a}
	{charge un registre avec une valeur}
    \instrdeux {mul} {imm/reg/dir/ind/idx} {reg}
	{mul \%b,\%a}
	{multiplie un registre par une valeur}
    \instrun {neg} {reg}
	{neg \%a}
	{négation d'un registre}
    \instrun {nop} {}
	{nop}
	{instruction n'effectuant aucune opération}
    \instrun {not} {reg}
	{not \%a}
	{\emph{non} bit à bit du registre}
    \instrdeux {or} {imm/reg/dir/ind/idx} {reg}
	{or \%a,\%b}
	{\emph{ou} bit à bit avec la valeur indiquée}
    \instrdeux {out} {imm/reg} {dir/ind/idx}
	{out 0x80,[50]}
	{écrit une valeur vers un contrôleur d'entrée/sortie}
    \instrun {pop} {reg}
	{pop \%b}
	{dépiler dans un registre et incrémenter \texttt{\%sp}}
    \instrun {push} {imm/reg}
	{push 5}
	{décrémenter \texttt{\%sp} et placer la valeur au sommet de la pile}
    \instrun {reset} {}
	{reset}
	{réinitialise le processeur}
    \instrun {rti} {}
	{rti}
	{retour d'interruption ou d'exception}
    \instrun {rtn} {}
	{rtn}
	{retour d'appel provoqué par \texttt{call}}

    \columnbreak

    \instrdeux {shl} {imm/reg/dir/ind/idx} {reg}
	{shl 2,\%a}
	{décalage à gauche des bits du registre}
    \instrdeux {shr} {imm/reg/dir/ind/idx} {reg}
	{shr \%b,\%a}
	{décalage à droite des bits du registre}
    \instrdeux{st} {reg} {dir/ind/idx}
	{st \%a,[\%b-4]}
	{stocke le contenu d'un registre en mémoire}
    \instrdeux {sub} {imm/reg/dir/ind/idx} {reg}
	{sub 0x20,\%a}
	{soustrait une valeur à un registre}
    \instrdeux {swap} {reg/dir/ind/idx} {reg}
	{swap [100],\%a}
	{permute une valeur et un registre}
    \instrun {trap} {}
	{trap}
	{provoque une exception de type \emph{trappe}}
    \instrdeux {xor} {imm/reg/dir/ind/idx} {reg}
	{xor [100],\%b}
	{\emph{ou exclusif} bit à bit avec la valeur indiquée}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB{Exemple : factorielle}

\begin{lstlisting}
// calcul de n! (n passé en argument)
factorielle:
    ld   [%sp+1],%a
    cmp  1,%a
    jge  casparticulier	// saut si 1 $\geq$ n
// cas général
    sub  1,%a		// a $\leftarrow$ n-1
    push %a
    call factorielle	// a $\leftarrow$ (n-1) !
    add  1,%sp		// dépile l'argument n-1
    push %b		// sauvegarder b
    ld   [%sp+2],%b	// b $\leftarrow$ n (argument original)
    mul  %b,%a		// a $\leftarrow$ n * (n-1)!
    pop  %b		// restaurer b
    rtn
casparticulier:
    ld   1,%a
    rtn
\end{lstlisting}


\titreB{Exemple : hello, world}

\begin{lstlisting}
#define	NBYTES	14	// nb d'octets dans la chaîne
#define	P_WRITE	7	// numéro de la primitive \texttt{write}
#define	P_EXIT	2	// numéro de la primitive \texttt{exit}
chaine:			// valeur du symbole = adr de la chaîne
    .string "hello, world!\n"
main:
    push NBYTES		// nombre d'octets à transférer
    push chaine		// empiler l'adresse de la chaîne
    push 1		// sortie standard
    ld   P_WRITE,%a	// numéro de la primitive
    trap		// appel à \texttt{write}
    add  3,%sp		// dépiler les arguments
    ld   P_EXIT,%a
    trap		// appel à \texttt{exit}
    rtn			// on ne devrait jamais arriver ici
\end{lstlisting}


\end {multicols}

\piedpage
\end {document}
