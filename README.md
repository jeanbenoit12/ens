Supports de cours de systèmes d'exploitation de Pierre David
============================================================

Bienvenue sur cette page qui regroupe mes divers supports de cours
(notamment de **systèmes d'exploitation** et **systèmes concurrents**
en licence et master d'informatique) à l'université de Strasbourg (France).


Licence
-------

Tous ces supports de cours sont placés sous licence « Creative Commons
Attribution - Pas d’Utilisation Commerciale 4.0 - International »

Pour accéder à une copie de cette licence, merci de vous rendre à
l'adresse suivante <https://creativecommons.org/licenses/by-nc/4.0/deed.fr>.

<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc.png" width="100">


Motivations
-----------

J'ai choisi de partager ces supports de cours pour plusieurs raisons :

- les étudiants préfèrent utiliser des versions électroniques des
    documents, et plutôt que laisser trainer des copies pirates
    obsolètes, je préfère directement mettre à disposition les
    versions à jour et originales.

- pour rédiger ces documents, outre mes travaux personnels et les
    lectures d'ouvrages, j'ai puisé dans les documents mis à disposition
    sur le réseau par d'autres enseignants que je ne saurais jamais
    trop remercier. C'est un juste retour des choses que d'offrir à
    mon tour ma modeste contribution, en espérant qu'elle soit utile
    à d'autres.

Si vous utilisez ces supports, que vous les trouvez utiles (que ce soit
juste pour les lire ou pour les adapter) et que vous voulez apporter un
peu de gentillesse dans ce bas monde, sentez-vous libre de m'envoyer un
petit mail à l'adresse pda (at) unistra.fr.


Erreurs, imperfections, coquilles, etc.
---------------------------------------

Si vous notez des erreurs, des imperfections, des coquilles ou n'importe
quoi qui permette d'améliorer ces supports, vous pouvez :

- me contacter par mail à l'adresse ci-dessus
- utiliser le mécanisme de [bug tracking de gitlab](https://gitlab.com/pdagog/ens/issues)
- ou, mieux encore si vous maîtrisez LaTeX et git, faire une « pull request »


Lecture en ligne
----------------

Si vous êtes intéressé-e-s par la lecture de ces supports, vous les
trouverez dans les liens suivants :

| Document | Objet |
| :------: | ----- |
| [manuel.pdf](https://pdagog.gitlab.io/ens/manuel.pdf) | notice simplifiée des primitives système et fonctions de librairie |
| [exercices.pdf](https://pdagog.gitlab.io/ens/exercices.pdf) | recueil d'exercices de programmation système |
| [conseils.pdf](https://pdagog.gitlab.io/ens/conseils.pdf) | conseils pour réussir TP et projets |
| [cours-ps.pdf](https://pdagog.gitlab.io/ens/cours-ps.pdf) | transparents du cours de programmation système de L2S4 (utilisation des primitives système) [[6 pages par page](https://pdagog.gitlab.io/ens/print-ps.pdf)] |
| [cours-ase.pdf](https://pdagog.gitlab.io/ens/cours-ase.pdf) | transparents du cours d'architecture des systèmes d'exploitation de L3S5 (voir aussi [z33refcard.pdf](https://pdagog.gitlab.io/ens/z33refcard.pdf)) [[6 pages par page](https://pdagog.gitlab.io/ens/print-ase.pdf)] |
| [z33refcard.pdf](https://pdagog.gitlab.io/ens/z33refcard.pdf) | aide mémoire de l'ordinateur Zorglub33 utilisé dans le cours d'ASE |
| [cours-cse.pdf](https://pdagog.gitlab.io/ens/cours-cse.pdf) | transparents du cours de conception de systèmes d'exploitation de Master 1 [[6 pages par page](https://pdagog.gitlab.io/ens/print-cse.pdf)] (voir aussi les exercices ci-dessous) |
| [exv6.pdf](https://pdagog.gitlab.io/ens/exv6.pdf) | exercices autour de xv6 (cours CSE) |
| [cours-reseau.pdf](https://pdagog.gitlab.io/ens/cours-reseau.pdf) | transparents du cours de réseaux de master 2 CSMI [[6 pages par page](https://pdagog.gitlab.io/ens/print-reseau.pdf)] |
| [cours-diu-eil.pdf](https://pdagog.gitlab.io/ens/cours-diu-eil.pdf) | transparents du cours de systèmes d'exploitation pour le diplôme inter-universitaire "Enseigner l'Informatique au Lycée" [[6 pages par page](https://pdagog.gitlab.io/ens/print-diu-eil.pdf)] |
| [memoire.pdf](https://pdagog.gitlab.io/ens/memoire.pdf) | conseils pour faire un bon mémoire de stage |
| [mem-ter.pdf](https://pdagog.gitlab.io/ens/mem-ter.pdf) | conseils pour faire un bon mémoire de TER |


Cours non maintenus
-------------------

Les cours suivants font partie d'anciennes maquettes et ne
sont plus activement maintenus :

| Document | Objet |
| :------: | ----- |
| [cours-sh.pdf](https://pdagog.gitlab.io/ens/cours-sh.pdf) | transparents du cours de pratique de systèmes d'exploitation de L2S3 (jusqu'en 2017/18) [[6 pages par page](https://pdagog.gitlab.io/ens/print-sh.pdf)] |
| [cours-se.pdf](https://pdagog.gitlab.io/ens/cours-se.pdf) | transparents du cours de systèmes d'exploitation de L2S4 (jusqu'en 2016/17) [[6 pages par page](https://pdagog.gitlab.io/ens/print-se.pdf)] |
| [cours-sc.pdf](https://pdagog.gitlab.io/ens/cours-sc.pdf) | transparents du cours de systèmes concurrents en L3S5 (jusqu'en 2018/19) [[6 pages par page](https://pdagog.gitlab.io/ens/print-sc.pdf)] |

Sources LaTeX
-------------

Si vous êtes intéressé-e-s par les sources de ces supports (pour
les reprendre tels quels ou les adapter à votre propre enseignement)
et si vous respectez les [conditions de la licence CC-BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/deed.fr),
vous trouverez dans [le dépôt gitlab](https://gitlab.com/pdagog/ens)
tout le nécessaire.

### Structure en répertoires

Les répertoires qui composent
[le dépôt gitlab](https://gitlab.com/pdagog/ens)
sont découpés de telle sorte
qu'ils soient autonomes : vous pouvez juste récupérer le répertoire
qui vous concerne sans vous soucier des autres.

### Prérequis

Pour compiler ces sources, il faut :

- une distribution de LaTeX complète (comme [texlive](https://www.tug.org/texlive/) par exemple), avec notamment les programmes `pdflatex` et `makeindex`
  ainsi que les paquetages LaTeX les plus courants
- les utilitaires Unix classiques, comme `make` (version GNU), `test`,
    `rm`, etc.
- les programmes :
  * `fig2dev` (venant avec [`xfig`](https://www.xfig.org), pour convertir les innombrables fichiers FIG en PDF)
  * [`inkscape`](https://inkscape.org) (pour convertir les fichiers SVG en PDF)
  * [`gnuplot`](http://www.gnuplot.info) (version 4.6 ou supérieure, pour le cours de systèmes concurrents notamment)
  * [`graphviz`](https://graphviz.org) (pour convertir les fichiers DOT en PDF, pour le cours d'architecture des systèmes d'exploitation)
  * [`pdfjam`](https://www2.warwick.ac.uk/fac/sci/statistics/staff/academic-research/firth/software/pdfjam) (si vous voulez imprimer les transparents en 3 ou 6 pages par page par exemple)

Pour imprimer les transparents en 3 ou 6 pages par page (cibles `print`
des `Makefile`), il faut le programme `pdfjam`.

Note spécifique au cours de système concurrents : certaines données
sont extraites de la CPUDB de Stanford. Une copie des données utilisées
est présente dans le dépôt. Si vous voulez rafraîchir ces données
(ce qui n'est à priori pas nécessaire), le moteur SGBD
[PostgreSQL](https://www.postgresql.org) sera indispensable.
