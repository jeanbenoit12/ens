main ()
{
  int sock ;
  struct sockaddr_in serv_addr ;

  sock = socket (PF_INET, SOCK_DGRAM, 0) ;

  bzero ((char *) &serv_addr, sizeof serv_addr) ;
  serv_addr.sin_family = AF_INET ;
  serv_addr.sin_addr.s_addr = htonl (INADDR_ANY) ;
  serv_addr.sin_port = htons (5000) ;

  bind (sock, &serv_addr, sizeof serv_addr) ;

  for (;;)
    serveur_udp (sock) ;
}

#define MAXMSG 1024

void serveur_udp (int sock)
{
  struct sockaddr_in cli_addr ;
  int n, cli_len ;
  char buf [MAXMSG] ;

  cli_len = sizeof cli_addr ;
  n = recvfrom (sock, buf, MAXMSG, 0, &cli_addr, &cli_len) ;
  sendto (sock, buf, n, 0, &cli_addr, cli_len) ; 
}
