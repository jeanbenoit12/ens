main ()
{
  int sock ;
  struct sockaddr_in serv_addr, cli_addr ;

  sock = socket (PF_INET, SOCK_DGRAM, 0) ;

  bzero ((char *) &cli_addr, sizeof cli_addr) ;
  cli_addr.sin_family = AF_INET ;
  cli_addr.sin_addr.s_addr = htonl (INADDR_ANY) ;
  bind (sock, &cli_addr, sizeof cli_addr) ;

  bzero ((char *) &serv_addr, sizeof serv_addr) ;
  serv_addr.sin_family = AF_INET ;
  serv_addr.sin_addr.s_addr = htonl (0x824fc805) ; // 130.79.200.5
  serv_addr.sin_port = htons (5000) ;

  client_udp (sock, &serv_addr) ;

  close (sock) ;
  exit (0) ;
}

#define MAXMSG 1024

void client_udp (int sock, struct sockaddr_in *serv_addr)
{
  char buf [MAXMSG] ;
  int n ;

  sendto (sock, buf, n, 0, serv_addr, sizeof *serv_addr) ;
  n = recvfrom (sock, buf, MAXMSG, 0, (struct sockaddr *) 0,
                                           (int *) 0) ;
}
