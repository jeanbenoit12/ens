void envoyer (datagramme_t datagramme) {
    IPdest = adresse_destination (datagramme) ;
    if ((IPdest & subnet_mask) == (IPmoi & subnet_mask)) {
	Eth = ARP (IPdest) ;
    } else {
	IProuteur = chercher_route (IPdest)
	if (non_trouve (IProuteur))
	    erreur () ;
	Eth = ARP (IProuteur) ;
    }
    encapsuler_trame (Eth, datagramme) ;
}

adresse_t chercher_route (adresse_t IPdest) {
    masque = 0xffffffff ;
    do {
	a = chercher_table (IPdest & masque) ;
	if (trouve (a))
	    return a ;
	masque <<= 1 ;
    } while (masque != 0) ;
    return -1 ;
}
