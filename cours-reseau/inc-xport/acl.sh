access-list 123 permit ip 198.18.0.0 0.0.127.255
			  198.51.100.0 0.0.0.255
access-list 123 permit ip any 198.51.100.1
access-list 123 deny   ip any 198.51.100.0 0.0.0.255

interface GigaEthernet5/23
  ip access-group 123 in
