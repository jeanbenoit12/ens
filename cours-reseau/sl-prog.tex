%
% Historique
%   92/12/?? : pda/jt : création
%   97/05/07 : pda    : conversion à latex2e et utilisation de pdasem
%

\incdir{inc-prog}

\titreA {Programmation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% L'API des sockets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {L'API des sockets}

\begin {frame} {Présentation}

    1981 : financement de l'Université de Berkeley par la DARPA\\
    \implique intégration des protocoles IP dans le noyau Unix

    \bigskip

    Initialement : 2 implémentations des protocoles IP

    \begin {itemize}
	\item Berkeley : adaptée aux réseaux locaux
	\item Bolt, Beranek et Newman (BBN) : adaptée aux réseaux longue
	    distance
    \end {itemize}

    Choix de la DARPA : implémentation BBN avec interface de
    programmation Berkeley.

\end{frame}

\begin {frame} {Présentation}

    Interface de programmation bien intégrée au système :

    \begin{itemize}
	\item développement facile de nouvelles applications
	\item émergence de nouveaux services au cours du temps \\
	    (Archie, Gopher, WAIS, WWW, NFS, P2P, etc.)
    \end{itemize}

\end{frame}


\begin {frame} {Présentation -- sockets}

    Interface de programmation = \emph{socket}

    \medskip

    \begin {center}
	\fig {sock-prc} {.7}
    \end {center}

\end{frame}

\begin {frame} {Présentation -- sockets}

    Les sockets sont apparues en 1982 avec BSD~4.1

    \begin {itemize}
	\item Les sockets sont un des mécanismes de communication
	    inter-processus disponibles sur Unix

	\item Les sockets ne sont pas liées à TCP/IP, et peuvent
	    utiliser d'autres protocoles (AppleTalk, Xerox XNS, etc.)

    \end {itemize}

    Autres interfaces concurrentes (\emph{Streams}, \emph{XTI}, etc.)~:
    obsolètes

\end{frame}



\begin {frame} {Présentation -- sockets}

    Objectif des sockets~:  préserver la sémantique des opérations sur
    les fichiers (\code{open}, \code{read}, \code{write}...).

    \bigskip

    Problème~:  certains mécanismes rendent nécessaires l'ajout d'appels
    systèmes complémentaires

\end{frame}


\begin {frame} {Présentation -- sockets}

    Utilisation des sockets~:

    \begin {enumerate}
        \item création de la socket~: \code{socket}

	\item établissement de la communication~:  \code{bind},
	       \code{connect} ou \code{listen}/\code{accept}

	\item échange de données~:  \code{read} et \code{write}
	      (socket = descripteur de fichier) ou \code{sendto} et
	      \code{recvfrom}

	\item fermeture de la communication~:  \code{close} ou
	      \code{shutdown}

    \end {enumerate}

    L'enchaînement des opérations dépend du mode de connexion~:

    \begin {itemize}
        \item mode connecté (exemple~: TCP)
        \item mode non connecté (exemple~: UDP)
    \end {itemize}

\end{frame}


\begin {frame} {Présentation -- familles de protocoles}

    \begin {itemize}
	\item les sockets ne sont pas liées à une famille de protocoles
	    donnée (IP, XNS, etc.)

	\item il existe certaines caractéristiques propres à chaque
	    famille de protocoles

	\item une socket est donc rattachée à une famille :
	    \ctableau {\fC} {|ll|} {
		\code{PF\_INET}  & Protocoles IPv4 \\
		\code{PF\_INET6} & Protocoles IPv6 \\
		\code{PF\_UNIX}  & Tubes nommés \\
		\code{PF\_NS}    & Xerox NS \\
		. . . & . . . \\
	    }

    \end {itemize}

\end{frame}


\begin {frame} {Présentation -- familles d'adresses}

    Les adresses sont propres à chaque famille~:

    \ctableau {\fC} {|ll|} {
	\code{AF\_INET}  & adresse IPv4 + numéro de port TCP ou UDP \\
	\code{AF\_INET6} & adresse IPv6 + numéro de port TCP ou UDP \\
	\code{AF\_UNIX}  & nom dans l'arborescence Unix \\
	. . . & . . . \\
    }

    \implique structure modèle pour les différentes familles :

    \lst {sockaddr.h} {\fC} {}

\end {frame}

\begin {frame} {Présentation -- familles d'adresses}
    \begin {center}
	\fig {sockaddr} {.7}
    \end {center}
\end {frame}

\begin {frame} {Présentation}

    La structure du champ \code{sa\_data} dépend de la famille de
    protocoles utilisée~:

    \begin {itemize}
	\item Famille \code{PF\_INET}

	    \lst {sockaddr_in.h} {\fC} {}

    \end {itemize}

\end {frame}

\begin {frame} {Présentation}

    \begin {itemize}
	\item Famille \code{PF\_INET6} (cf \code{netinet/in.h})

	\lst {sockaddr_in6.h} {\fC} {}

    \end {itemize}

\end{frame}


\begin {frame} {Présentation}

    \begin {itemize}
	\item Famille \code{PF\_UNIX} (cf \code{sys/un.h})

	    \lst {sockaddr_un.h} {\fC} {}

    \end {itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mode connecté
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Mode connecté}

\begin {frame} {Mode connecté -- Principe}

    \begin {center}
	\fig {sock-tcp} {.4}
    \end {center}

\end{frame}


\begin {frame} {Mode connecté -- socket}

    \textbf {Rôle}~: crée la socket côté client et serveur

    \textbf {Syntaxe}~:\\
	{\fC
	\code{
	    int socket (int famille, int type, int proto)
	} }

    \medskip

    \code{socket} renvoie un descripteur de socket ($\Leftrightarrow$
    fichier) utilisable avec les autres primitives

    \begin {itemize}

	\item \code{type} = type de connexion :

	    \ctableau {\fD} {|ll|c|c|} {
		    \multicolumn {2} {|c|} {\code{type}}
			& \multicolumn {2} {c|} {\code{famille}}
			\\
		    \rca & &
			\code{PF\_UNIX} &
			\code{PF\_INET/INET6} \\
		    \rcb \code{SOCK\_STREAM} & (connecté) &
			Oui & TCP \\
		    \rca \code{SOCK\_DGRAM} & (non connecté) &
			Oui & UDP \\
		    \rcb \code{SOCK\_RAW} & (brut) &
			~ & IP  \\
	    }

	    \medskip

	\item \code{protocole} = numéro du protocole (IP, TCP,
	    UDP, etc.) \\
	    (0 \implique choix par le système)

    \end {itemize}

\end{frame}


\begin {frame} {Mode connecté -- bind}

    \textbf {Rôle} :
    attache une adresse (IP + port) à une socket

    \textbf {Syntaxe} : \\
	{\fC
	\code{
	    int bind (int s, struct sockaddr *adr, int lg)
	} }

    \medskip

    Numéro de port = 0 \implique choix laissé au système

\end{frame}




\begin {frame} {Mode connecté -- listen}

    \textbf {Rôle} :
    place la socket en mode «~ouverture passive~» et définit
    la longueur de la file d'attente des connexions. \\
    (ouverture passive \implique pour le serveur exclusivement)

    \textbf {Syntaxe} : \\
	{\fC
	\code{
	    int listen (int s, int longueur)
	} }

\end{frame}

\begin {frame} {Mode connecté -- accept}

    \textbf {Rôle} :
    accepte une connexion en provenance d'un client \\
    (pour le serveur exclusivement)

    \textbf {Syntaxe}~: \\
	{\fC
	\code{
	    int accept (int s, struct sockaddr *adr, int *lg)
	} }

    \medskip

    Après \code{accept} :
    \begin {itemize}
	\item l'appel système retourne un nouveau descripteur de socket
	    utilisé pour le dialogue avec le client

	\item \code{adr} : adresse du client connecté

	\item \code{lg} : longueur de l'adresse
    \end {itemize}

\end{frame}

\begin {frame} {Mode connecté -- connect}

    \textbf {Rôle} :
    connexion au serveur.\\
    (pour le client exclusivement)

    \textbf {Syntaxe}~: \\
	{\fC
	\code{
	    int connect (int s, struct sockaddr *adr, int lg)
	} }

    \begin {itemize}
	\item \code{adr} :  adresse de l'application (IP + port)

	\item \code{lg} :  longueur de l'adresse
    \end {itemize}

\end{frame}

\begin {frame} {Mode connecté -- Serveur IPv4}
    \lst {tcpserv4.c} {\fD} {lastline=23}
\end{frame}

\begin {frame} {Mode connecté -- Serveur IPv4}
    \lst {tcpserv4.c} {\fD} {firstline=25}
\end{frame}

\begin {frame} {Mode connecté -- Client IPv4}
    \lst {tcpcli4.c} {\fD} {lastline=19}
\end{frame}

\begin {frame} {Mode connecté -- Client IPv4}
    \lst {tcpcli4.c} {\fD} {firstline=21}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Mode non connecté
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Mode non connecté}

\begin {frame} {Mode non connecté -- Principe}

    \begin {center}
	\fig {sock-udp} {.4}
    \end {center}

\end{frame}

\begin {frame} {Mode non connecté}

    \begin {enumerate}
	\item le serveur et les clients créent chacun une
	    socket (\code{SOCK\_DGRAM})

	\item le serveur utilise \code{bind} avec un numéro 
	    de port unique

	\item chaque client doit utiliser une adresse (IP + port)
	    différente

	\item le dialogue se fait par \code{sendto} et \code
	    {rcvfrom}

    \end {enumerate}
\end{frame}

\begin {frame} {Mode non connecté -- sendto}

    \textbf {Rôle} :
    envoyer un message en mode non connecté

    \textbf {Syntaxe}~: \\
	{\fC
	\code{
	    sendto (int s, char *buf, int nb, int flags, \\
	    \hspace* {30mm} struct sockaddr *to, int len)
	} }

    \begin {itemize}
	\item
	    \code{sendto} retourne le nombre de caractères envoyés

	\item
	    \code{s}, \code{buf} et \code{nb} : idem \code{write}

	\item
	    \code{flag} peut prendre les valeurs :

	    \ctableau {\fC} {|ll|} {
		\code{0} & envoi messages normaux \\
		\code{MSG\_OOB} & envoi messages spéciaux \\
		\code{MSG\_DONTROUTE} & pas de routage \\
	    }

	\item
	    \code{to} : adresse et port auxquels on envoie le message

    \end {itemize}

\end{frame}

\begin {frame} {Mode non connecté -- rcvfrom}

    \textbf {Rôle} :
    recevoir un message en mode non connecté.

    \textbf {Syntaxe}~: \\
	{\fC
	\code{
	    rcvfrom (int s, char *buf, int nb, int flags, \\
	    \hspace* {30mm}
		     struct sockaddr *from, int *len)
	} }

    \begin {itemize}
	\item
	    \code{rcvfrom} retourne le nombre de caractères reçus

	\item
	    \code{s}, \code{buf} et \code{nb} : idem \code{read}

	\item
	    \code{flag} peut prendre les valeurs :

	    \ctableau {\fC} {|ll|} {
		\code{0} & réception messages normaux \\
		\code{MSG\_OOB} & réception messages spéciaux \\
		\code{MSG\_PEEK} & regarder le message \\
	    }

	\item
	    \code{from} : adresse et port d'où vient le message

    \end {itemize}

\end{frame}

\begin {frame} {Mode non connecté -- Serveur IPv4}
    \lst {udpserv4.c} {\fD} {lastline=17}
\end{frame}

\begin {frame} {Mode non connecté -- Serveur IPv4}
    \lst {udpserv4.c} {\fD} {firstline=19}
\end{frame}

\begin {frame} {Mode non connecté -- Client IPv4}
    \lst {udpcli4.c} {\fD} {lastline=22}
\end{frame}

\begin {frame} {Mode non connecté -- Client IPv4}
    \lst {udpcli4.c} {\fD} {firstline=24}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Diagnostics
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Diagnostics}

\begin {frame} {Diagnostics}

    En cas d'erreur, les primitives systèmes renvoient -1 en code de
    retour, et un numéro d'erreur dans la variable globale \code{errno}

    De nouvelles définitions sont nécessaires pour gérer les protocoles
    IP.
\end{frame}


\begin {frame} {Diagnostics}

    \ctableau {\fE} {|ll|} {
	\code{EADDRINUSE}	& adresse déjà utilisée \\
	\code{EADDRNOTAVAIL}	& l'adresse ne peut être affectée \\
	\code{EAFNOSUPPORT}	& famille d'adresses non supportée \\
	\code{ECONNABORTED}	& connexion rompue \\
	\code{ECONNREFUSED}	& connexion refusée \\
	\code{ECONNRESET}	& connexion rompue par l'autre extrémité \\
	\code{EDESTADDRREQ}	& adresse de destination requise \\
	\code{EHOSTDOWN}	& machine ne répondant pas \\
	\code{EHOSTUNREACH}	& aucune route trouvée \\
	\code{EINPROGRESS}	& opération en cours \\
	\code{EISCONN}	& socket déjà connectée \\
	\code{ENET}		& erreur du logiciel ou du matériel réseau \\
	\code{ENETDOWN}	& réseau hors service \\
	\code{ENETRESET}	& connexion coupée par le réseau \\
	\code{ENETUNREACH}	& pas de route vers le réseau \\
	\code{ENOPROTOOPT}	& protocole non disponible  \\
	\code{ENOTCONN}	& socket non connectée \\
	\code{ENOTSOCK}	& opération sur une socket \\
	\code{EPROTONOSUPPORT}	& protocole non supporté \\
	\code{EPROTOTYPE}	& mauvais type pour la socket \\
	\code{ESHUTDOWN}	& transmission après un {\tt shutdown} \\
	\code{ESOCKTNOSUPPORT}	& type de socket non supporté \\
	\code{ETIMEDOUT}	& temps d'attente dépassé \\
    }

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FONCTIONS UTILITAIRES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Fonctions utilitaires}

\begin {frame} {Présentation}

    Les primitives systèmes utilisent~:

    \begin {itemize}
	\item des adresses IPv4 sur 32 ou IPv6 sur 128 bits
	\item des numéros de port sur 16 bits
	\item des numéros de protocole
    \end {itemize}

\end{frame}

\begin {frame} {Présentation}
    Utiliser des noms plutôt que des valeurs numériques~?

    \begin {center}
	\fig {fct-prc} {.6}
    \end {center}

    \begin {itemize}
	\item machines~: www.unistra.fr, mailhost.u-strasbg.fr...

	\item ports~: smtp/tcp, domain/udp...

	\item protocoles~: IP, TCP...

    \end {itemize}

\end{frame}

\begin {frame} [fragile] {Numéros de port}

    Fichier \code{/etc/services} : traduction nom $\leftrightarrow$ numéro
    de port

    \medskip

    Format~: \\
    \code{service port/protocole [synonymes...]}

    \ctableau {\fC} {|ll|} {
	\code{service}        & nom officiel du service \\
	\code{port/protocole} & numéro de port et protocole \\
	\code{synonymes}      & liste de synonymes \\
    }

    Exemple :
    \begin {lstlisting}
    ftp    21/tcp
    telnet 23/tcp
    smtp   25/tcp  mail
    domain 53/tcp  nameserver
    domain 53/udp  nameserver
    tftp   69/udp
    \end{lstlisting}
\end{frame}


\begin {frame} {Numéros de port -- getservbyname}

    \textbf {Rôle} :
    obtenir le numéro d'un port à partir de son nom

    \textbf {Syntaxe}~: \\
	{\fC
	\code{
	    struct servent *getservbyname (char *nom, char *prot)
	} }

    \medskip

    La structure \code{servent} est définie comme :

    \lst {servent.h} {\fD} {}

    Si \code{proto} est spécifié, le protocole doit être défini dans
    le fichier \code{/etc/protocols}

\end{frame}


\begin {frame} [fragile] {Numéros de protocole}

    Fichier \code{/etc/protocols} : traduction nom $\leftrightarrow$
    numéro de protocole

    \medskip

    Format : \\
    \code{protocole numéro [synonymes...]}

    \ctableau {\fC} {|ll|} {
	\code{protocole}  & nom officiel du protocole \\
	\code{numéro}     & numéro de protocole \\
	\code{synonymes}  & liste de synonymes \\
    }

    Exemple :
    \begin {lstlisting}
    ip    0   IP
    icmp  1   ICMP
    tcp   6   TCP
    udp   17  UDP
    \end{lstlisting}
\end{frame}


\begin {frame} {Numéros de port -- getprotobyname}

    \textbf {Rôle} :
    obtenir le numéro du protocole à partir de son nom.

    \textbf {Syntaxe}~: \\
	{\fC
	\code{
	    struct protoent *getprotobyname (char *nom)
	} }

    \medskip
    La structure \code{protoent} est définie comme :
    \lst {protoent.h} {\fD} {}
\end{frame}

\begin {frame} [fragile] {Adresses IP -- /etc/hosts}

    Fichier \code{/etc/hosts} : utilisé quand le serveur de noms n'est pas
    actif (ou en complément)

    \medskip

    Format~: \\
	\code{adresse nom-officiel [synonymes...]}

    \ctableau {\fC} {|ll|} {
	\code{adresse}      & adresse IPv4 ou IPv6 \\
	\code{nom-officiel} & nom de la machine \\
	\code{synonymes}    & synonymes facultatifs \\
    }

    \medskip
    Exemple :

    \begin {lstlisting}
    130.79.201.195    www.unistra.fr    www
    130.79.200.5      ftp.u-strasbg.fr  ftp
    2001:660:2402::6  ftp.u-strasbg.fr  ftp
    \end{lstlisting}

\end{frame}

\begin {frame} [fragile] {Adresses IP -- DNS}

    Fichier \code{/etc/resolv.conf} : utilisé pour identifier le
    serveur de noms mandataire et le ou les critères de \emph{domain
    completion}

    \medskip

    Exemple :
    \begin {lstlisting}
    search u-strasbg.fr
    nameserver 130.79.200.200
    nameserver 2001:660:2402::200
    \end{lstlisting}

\end{frame}


\begin {frame} {Informations de connexion -- getaddrinfo}

    \textbf {Rôle} 
    obtenir les informations de connexion (adresses IP et numéro de port)

    \textbf {Syntaxe}~:\\
	{\fC
	\code{
	    int getaddrinfo (char *nom, char *service, \\
	    \hspace* {20mm}
	    	struct addrinfo *indic,
	    	struct addrinfo **res0)
	} }

    \medskip

    La structure \code{addrinfo} est définie comme :

    \lst {addrinfo.h} {\fD} {}

\end{frame}


\begin {frame} {Informations de connexion -- getaddrinfo}

    Utilisation par un client pour se connecter à un serveur~:

    \begin {center}
	\fig {getai-cli} {.8}
    \end {center}
\end{frame}


\begin {frame} {Informations de connexion -- getaddrinfo}

    Code correspondant~:
    
    \begin {itemize}
	\item le client doit parcourir la liste pointée par \code
	    {res0} et tenter une connexion sur chaque adresse

	\item il faut s'arrêter à la première connexion réussie

	\item ne pas oublier d'utiliser \code{freeaddrinfo}
	    pour désallouer la liste

    \end {itemize}
\end{frame}


\begin {frame} {Informations de connexion -- getaddrinfo}
    \lst {getai-cli.c} {\fD} {}
\end{frame}

\begin {frame} {Informations de connexion -- getaddrinfo}
    Utilisation par un serveur pour ouvrir la socket~:

    \begin {center}
	\fig {getai-srv} {.8}
    \end {center}
\end{frame}

\begin {frame} {Informations de connexion -- getaddrinfo}
    Code correspondant~:
    
    \begin {itemize}
	\item le serveur doit créer une nouvelle socket de connexion
	    passive pour chaque élément de la liste

	\item le serveur doit détecter une tentative sur n'importe
	    laquelle des sockets
	    \\
	    \implique primitive système \code{select}

	\item ne pas oublier d'utiliser \code{freeaddrinfo}
	    pour désallouer la liste après avoir créé les sockets

    \end {itemize}
\end{frame}

\begin {frame} {Informations de connexion -- getaddrinfo}
    \lst {getai-srv.c} {\fD} {firstline=45, lastline=72}
\end{frame}

\begin {frame} {Informations de connexion -- getaddrinfo}
    \lst {getai-srv.c} {\fD} {firstline=74, lastline=99}
\end{frame}

\begin {frame} [fragile] {Macros de test}

    \begin {lstlisting}
int IN6_IS_ADDR_UNSPECIFIED (struct in6_addr *) ;
int IN6_IS_ADDR_LOOPBACK    (struct in6_addr *) ;
int IN6_IS_ADDR_MULTICAST   (struct in6_addr *) ;
int IN6_IS_ADDR_V4COMPAT    (struct in6_addr *) ;
int IN6_IS_ADDR_V4COMPAT    (struct in6_addr *) ;
...
    \end{lstlisting}

\end{frame}

% \begin {frame} {Programmation -- Hop Limit}
% 
%     Champ \emph{Hop Limit} dans l'en-tête IPv6 est configurable par
%     l'application (pour les adresses unicast)
% 
%     \implique option \code{IPV6\_UNICAST\_HOPS} de \code{setsockopt}
% 
% \end {frame}
% 
% \begin {frame} {Programmation -- Qualité de service}
% 
%     Le \emph{flow label} et \emph{traffic class} (sur 28 bits)
%     sont spécifiés dans la structure \code{sockaddr\_in6}.
% 
%     Cette valeur est prise en compte~:
% 
%     \begin {itemize}
% 	\item pour les connexions TCP~: \\
% 	    avec
% 	    \code{bind} (ouverture passive) et
% 	    \code{connect} (ouverture active)
% 	\item pour les paquets UDP~: \\
% 	    avec \code{sendto}
%     \end {itemize}
% 
%     Ce champ \emph{flow info} doit être initialisé à 0 si on ne
%     veut pas le spécifier
% 
% 
% \end {frame}
% 
% \begin {frame} {Programmation -- Routage explicite}
% 
%     Astuce pour spécifier le routage explicite~: \\
%     \implique utiliser un tableau de \code{sockaddr\_in6} au lieu d'une seule
% 
%     \medskip
% 
%     \textbf {Émission}
% 
%     Pour spécifier un routage explicite~:
% 
%     \begin {itemize}
% 	\item avec une connection TCP~: \code{connect}
% 	\item pour un paquet UDP~: \code{sendto}
%     \end {itemize}
% 
%     \textbf {Réception}
% 
%     Par défaut, le routage explicite n'est pas transmis lorsque des
%     données sont reçues \\
%     \implique utiliser l'option \code{IP\_RCVSRCRT} de \code{setsockopt}
%     \\
%     Le routage explicite est alors transmis~:
% 
%     \begin {itemize}
% 	\item avec une connection TCP~: \code{accept}
% 	\item pour un paquet UDP~: \code{recvfrom}
%     \end {itemize}
% \end {frame}
% 
% \begin {frame} {Programmation -- Multicast}
% 
%     Multicast = adresse dans un paquet UDP \implique \code{sendto}.
% 
%     \medskip
% 
%     \textbf {Émission}
% 
%     Options de \code{setsockopt} pour contrôler l'émission de
%     paquets multicast~:
% 
%     \begin {itemize}
% 	\item \code{IPV6\_MULTICAST\_IF}~:\\
% 	    autorise l'émission de paquets multicast
% 	\item \code{IPV6\_MULTICAST\_HOPS}~:\\
% 	    initialise le nombre de saut maximum
% 	\item \code{IPV6\_MULTICAST\_LOOP}~:\\
% 	    l'application doit recevoir une copie des paquets sortants
%     \end {itemize}
% \end {frame}
% 
% \begin {frame} {Programmation -- Multicast}
% 
%     \textbf {Réception}
% 
%     Options de \code{setsockopt} pour contrôler la réception de
%     paquets multicast~:
% 
%     \begin {itemize}
% 	\item \code{IPV6\_JOIN\_GROUP}~:\\
% 	    l'application veut recevoir les paquets à
% 	    l'adresse spécifiée
% 	\item \code{IPV6\_LEAVE\_GROUP}~:\\
% 	    l'application ne veut plus recevoir les paquets
%     \end {itemize}
% 
% \end {frame}


% \begin {frame}
%     \titre {Programmation - Sécurité}
% 
%     Comment programmer une application sécuritaire~?
% 
%     \begin {enumerate}
% 	\item spécifier le niveau de sécurité désiré \\
% 	    \implique option {\tt IP\_SECLEVEL} de {\tt setsockopt}
% 
% 	\item spécifier quelles erreurs doivent être signalées \\
% 	    \implique option {\tt IP\_SECERR} de {\tt setsockopt}
% 
% 	\item accepter et traiter le signal {\tt SIGURG} \\
% 	    \implique pour recevoir les erreurs tout de suite
% 
% 	\item écrire l'application en testant le retour des primitives \\
% 
% 	\item lorsque le signal {\tt SIGURG} arrive~:
% 
% 	    \begin {itemize}
% 		\item chercher si origine = problème de sécurité \\
% 		    \implique option {\fB\tt IP\_SECEXCEPT} de
% 		    {\fB\tt getsockopt}
% 		\item obtenir la cause précise de l'erreur \\
% 		    \implique option {\fB\tt IP\_SECMSG} de
% 		    {\fB\tt getsockopt}
% 	    \end {itemize}
%     \end {enumerate}
% 
%     Notion de niveau de sécurité pour le système~: \\
%     si une application demande un niveau moins sécuritaire que
%     le niveau général du système, ce dernier prévaut.
% \end {frame}
% 
% 
% \begin {frame}
%     \titre {Programmation - Sécurité}
% 
%     Option {\tt IP\_SECLEVEL} de {\tt setsockopt} \\
%     \implique associe à la \emph{socket} un niveau de sécurité
%     (authentification et/ou chiffrement des paquets en sortie)
% 
%     Authentification des paquets en sortie~:
% 
%     \begin {tabular} {|ll|} \hline
% 	\fC\tt IPSEC\_AUTHNONE &
% 	    pas d'authentification
% 	    \\
% 	\fC\tt IPSEC\_AUTHCRIT &
% 	    paquets «~critiques~» seulement
% 	    \\
% 	    &
% 	    \implique (ex. début de connexion)
% 	    \\
% 	\fC\tt IPSEC\_AUTHALL &
% 	    tous les paquets
% 	    \\
% 	\hline
%     \end {tabular}
% 
%     Chiffrement des paquets en sortie~:
% 
%     \begin {tabular} {|ll|} \hline
% 	\fC\tt IPSEC\_ESPNONE &
% 	    pas de chiffrement
% 	    \\
% 	\fC\tt IPSEC\_ESPPAYLOAD &
% 	    contenu
% 	    \\
% 	\fC\tt IPSEC\_ESPPACKET &
% 	    contenu et en-tête
% 	    \\
% 	\fC\tt IPSEC\_ESPAUTHPACKET &
% 	    authentifie puis chiffre tout
% 	    \\
% 	    \hline
%     \end {tabular}
% 
% \end {frame}
% 
% 
% \begin {frame}
%     \titre {Programmation - Sécurité}
% 
%     Option {\tt IP\_SECERR} de {\tt setsockopt} \\
%     \implique spécifie quel type d'erreur doit être retourné
% 
%     \begin {tabular} {|ll|} \hline
% 	Valeur & Retourne une erreur si...
% 	    \\ \hline
% 	\fC\tt IPSEC\_REPFAIL &
% 	    échec de l'authentification
% 	    \\
% 	\fC\tt IPSEC\_REPNAKED &
% 	    reçu un paquet «~nu~» (non
% 	    \\
% 	    &
% 	    authentifié) alors qu'il devrait l'être
% 	    \\
% 	\fC\tt IPSEC\_REPPARANOID &
% 	    champ \emph{Association Id} invalide
% 	    \\
% 	    \hline
% 	    \hline
% 	Valeur & Action
% 	    \\ \hline
% 	\fC\tt IPSEC\_PASSPARANOID &
% 	    accepte paquets «~paranoïaques~»
% 	    \\
% 	    \hline
%     \end {tabular}
% 
% \end {frame}
% 
% 
% \begin {frame}
%     \titre {Programmation - Sécurité}
% 
%     Nouvelles erreurs renvoyées par les primitives système~:
% 
%     \begin {itemize}
% 	\item {\tt ENOKEY}~: niveau de sécurité demandé impossible \\
% 	    ex~: machine distante ne supporte pas l'authentification
% 	\item {\tt EKEYEXP}~: la clef est périmé
%     \end {itemize}
% 
% \end {frame}
% 
% 
% \begin {frame}
%     \titre {Programmation - Sécurité}
% 
%     Les erreurs de sécurité sont principalement retournées
%     par le signal {\tt SIGURG} \implique rapport immédiat
% 
%     Options disponibles dans la fonction associée au signal~:
% 
%     \begin {enumerate}
% 	\item chercher l'origine du problème de sécurité \\
% 	    \implique option {\tt IP\_SECEXCEPT} de {\tt getsockopt}
% 
% 	    \begin {tabular} {|ll|} \hline
% 		\fC\tt IPSECEXC\_NONE &
% 		    pas de problème de sécurité
% 		    \\
% 		\fC\tt IPSECEXC\_FAIL &
% 		    problème d'authentification
% 		    \\
% 		\fC\tt IPSECEXC\_NAKED &
% 		    reçu un paquet «~nu~»
% 		    \\
% 		\fC\tt IPSECEXC\_PARANOID &
% 		    reçu un paquet «~paranoïaque~»
% 		    \\
% 		\hline
% 	    \end {tabular}
% 
% 
% 	\item obtenir la cause précise de l'erreur \\
% 	    \implique option {\tt IP\_SECMSG} de {\tt getsockopt}
% 
% 	    Retourne des informations détaillées sur la sécurité du
% 	    message.
% 
%     \end {enumerate}
% 
% \end {frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INTEGRATION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\titreB {Intégration}

\begin {frame} {Démons}

    Les serveurs sont réalisés sous Unix grâce à des \emph{démons}

    \medskip

    Processus utilisateurs ordinaires, mais :

    \begin {itemize}
	\item fonctionnement en arrière plan (non lié à un terminal)
	\item souvent avec les droits du super-utilisateur
	\item lancés en général au démarrage du système
	\item ne s'arrêtent jamais
    \end {itemize}
\end{frame}

\begin {frame} {Démons}

    Les démons doivent suivre certaines règles de conception pour
    répondre aux contraintes :

    \begin {enumerate}
	\item faire un \code{fork}, terminer le père et continuer
	    dans le fils \\
	    \implique fils «~orphelin~»
	    \implique nouveau père = processus 1 (\code{init})

	\item appeler \code{setsid} \\
	    \implique créer une nouvelle session, non rattachée à
	    un terminal

	\item changer le répertoire courant pour un répertoire qui
	    existe toujours (p. ex. : \code{/})

	\item modifier le masque de création des fichiers
	    (\code{umask}~=~0) \\
	    \implique expliciter les permissions des fichiers créés

	\item fermer les descripteurs de fichiers inutiles
    \end {enumerate}

\end{frame}

\begin {frame} {Démons}

    Problème~: plus de terminal pour les messages

    Solution~:  envoi des messages par le démon \code{syslogd}

    \begin {center}
	\fig {syslog} {.6}
    \end {center}
\end{frame}


\begin {frame} {Démons}
    \lst {demon.c} {\fC} {}
\end{frame}

\begin {frame} {Internet Super Server}

    Problèmes~:

    \begin {itemize}
	\item chaque démon doit avoir une séquence de code quasiment
	    toujours identique pour accepter les connexions

	\item les démons sont des processus qui utilisent des ressources
	    même s'ils sont inactifs

    \end {itemize}

    Solution~:

    \begin {itemize}
	\item remplacer tous les démons réseau correspondant aux divers
	    services par un seul
	\item \implique diminuer le nombre de processus
	\item \implique simplifier l'écriture des démons
	\item implémentation «~historique~» : \code{inetd}
	\item autres implémentations : \code{xinetd}, \code{launchd},
	    \code{systemd}, etc.
    \end {itemize}

\end{frame}

\begin {frame} {Internet Super Server -- inetd}

    Implémentation~:

    \begin {itemize}
	\item
	    Le fichier \code{/etc/inetd.conf} liste les démons gérés par
	    \code{inetd}

	\item
	    \code{inetd} utilise \code{bind}/\code{listen}/\code
	    {connect} pour chaque service

	\item
	    Après une connexion d'un client à un de ces services, \code
	    {accept} et \code{exec} du serveur

    \end {itemize}

\end{frame}

\begin {frame} [fragile] {Internet Super Server -- inetd}

    Le fichier \code{inetd.conf} contient pour chaque service :

    \ctableau {\fD} {|ll|} {
	nom du service		& présent dans \code{/etc/services} \\
	type			& \code{stream} ou \code{dgram} \\
	protocole		& dans \code{/etc/protocols} \\
	\code{wait} / \code{nowait}   & \code{wait} (ou non) en mode stream \\
	user			& en général : root \\
	nom du serveur		& nom complet \\
	arguments		& au plus 5 \\
    }

    Exemple :

{\fD
\begin {lstlisting}
ftp    stream tcp nowait root /etc/ftpd    ftpd -l
telnet stream tcp nowait root /etc/telnetd telnetd
ntalk  dgram  udp wait   root /etc/ntalkd  ntalkd
\end{lstlisting}
}
\end{frame}


\begin {frame} {Internet Super Server}

    Un serveur (exemple pour TCP) avec \code{inetd} est réduit à~:

    \lst {inetd.c} {\fC} {}

\end {frame}

\begin {frame} {Internet Super Server}

    Aujourd'hui, l'Internet Super Server (\code{inetd} ou équivalent)
    n'est plus réellement utilisé :

    \begin{itemize}
	\item \code{inetd} provoque un appel à \code{fork} et à
	    \code{exec} à chaque connexion
	    \\
	    \implique peu efficace pour des connexions de courte durée
	    (ex : HTTP)

	\item la plupart des services conçus avec \code{inetd} sont
	    obsolètes car :
	    \begin{itemize}
		\item services non authentifiés pour la plupart
		\item services sans chiffrement
		\item volonté de réduire la surface d'attaque
	    \end{itemize}

	\item la tendance est de faire des \emph{micro-services}
	    \begin{itemize}
		\item services élémentaires
		\item durée de connexion très courte
		\item utilisant HTTP comme protocole support
	    \end{itemize}
	    \implique utilisation de serveurs spécialisés dans ce type
	    de traitement

    \end{itemize}

\end {frame}
