> dig ns unistra.fr @d.nic.fr	# interrogation de type NS envoyée au serveur de .fr
...
;; Got answer:			# chic, on a reçu une réponse !
# informations sur la réponse : flags du paquet, nombre de RR dans chaque section (à noter ici : aucune réponse)
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 3, ADDITIONAL: 6

;; QUESTION SECTION:		# rappel de la question (première partie du paquet)
;unistra.fr.		 IN NS

;; AUTHORITY SECTION:		# les informations d'autorité (3e partie du paquet)
unistra.fr.	  172800 IN NS	 ns2.u-strasbg.fr.
unistra.fr.	  172800 IN NS	 ns1.u-strasbg.fr.
unistra.fr.	  172800 IN NS	 shiva.jussieu.fr.

;; ADDITIONAL SECTION:		# les informations additionnelles (4e partie du paquet)
ns2.u-strasbg.fr. 172800 IN AAAA 2001:660:2402::3
ns1.u-strasbg.fr. 172800 IN AAAA 2001:660:2402::1
shiva.jussieu.fr. 172800 IN A	 134.157.0.129
ns2.u-strasbg.fr. 172800 IN A	 130.79.200.3
ns1.u-strasbg.fr. 172800 IN A	 130.79.200.1

;; Query time: 12 msec
;; SERVER: 2001:678:c::1#53(d.nic.fr) (UDP)
