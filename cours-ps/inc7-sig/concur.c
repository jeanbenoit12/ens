// insertion en tête de liste simplement chaînée
nouveau->suivant = tete ;
tete = nouveau ;

// lecture puis modification d'un fichier
read (fd, &val, sizeof val) ;
val = val + ... ;
lseek (fd, SEEK_SET, 0) ;
write (fd, &val, sizeof val) ;

// prise de décision puis modification
if (x == 25) {
    x++ ;
}
