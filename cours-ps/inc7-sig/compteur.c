// un gros compteur pour compter au delà de $2^{32}$
struct gros_compteur {
    uint32_t tresgrand ;
    uint32_t grand ;
} c = {0, 0} ;

void incrementer (void) {
    if (c.grand == UINT32_MAX) {
	c.grand = 0 ;
	c.tresgrand++ ;
    }
    else c.grand++ ;
}
void f (int sig) {	// partie asynchrone
    incrementer () ;
}

int main (...) {	// partie synchrone
    struct sigaction s ;
    s.sa_handler = f ;
    ...
    sigaction (SIGINT, &s, NULL) ;
    ...
    while (...) {
	incrementer (); // calculs
    }
}
