void raler (const char *msg)	// fonction simple à utiliser au maximum !
{
    perror (msg) ;
    exit (1) ;
}

...

fd = open ("toto", O_RDONLY) ;
if (fd == -1)
    raler ("open toto") ;

...
