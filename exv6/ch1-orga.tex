\td {Organisation du système}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\question

Cet exercice est un préalable à tous les exercices où vous aurez à
modifier du code de xv6. L'utilisation de \texttt{git} et d'un dépôt
privé vous permet de facilement repérer vos modifications et d'y
accéder depuis n'importe quel ordinateur.

\begin {enumerate}
    \item Commencez par \emph{cloner} le
	dépôt original du MIT, 
	\url{https://github.com/mit-pdos/xv6-public}

	Attention : ne vous contentez pas de télécharger les sources,
	mais utilisez le bouton \framebox{Code}, recopiez l'URL de
	clonage et utilisez-la avec \texttt{git clone}.

	Astuce : vous pouvez utiliser l'utilitaire graphique \texttt{gitk}
	(paquet Ubuntu \texttt{gitk}) pour naviguer dans l'historique
	du dépôt. Si vous n'avez pas cloné correctement le dépôt,
	\texttt{gitk} vous l'indiquera.

    \item Si vous ne l'avez pas déjà fait, il est temps
	d'installer votre clef SSH sur votre compte
	\url{https://gitlab.unistra.fr} sinon vous devrez saisir votre
	mot de passe à chaque fois que vous accédez à votre dépôt.

    \item Créez ensuite un dépôt privé (vide) sur
	\url{https://gitlab.unistra.fr} (nouveau «~projet~» dans la
	terminologie gitlab)

    \item Vous pouvez ensuite suivre les instructions affichées pour
	configurer votre identité et pousser le dépôt \texttt{git}
	existant (que vous venez de récupérer à l'étape 1) sur
	gitlab.unistra.fr

	Vérifiez avec \texttt{git remote -v} que l'origine est bien
	votre dépôt sur \texttt{gitlab.unistra.fr}

    \item Pour conserver un point de référence stable, il vous est
	suggéré de faire vos modifications uniquement dans des branches
	dédiées. Pour créer et utiliser une branche, il faut utiliser
	les commandes suivantes~:

	\begin {quote}
	\begin {tabular} {ll}
	    \texttt{git branch toto}
		& pour créer une branche \texttt{toto} locale
		\\
	    \texttt{git checkout toto}
		& pour basculer sur la branche \texttt{toto} locale
		\\
	    \texttt{git push -{}-all}
		& pour propager la branche locale vers
		\texttt{gitlab.unistra.fr}
	\end {tabular}
	\end {quote}

	Modifiez le fichier \texttt{README}, puis commitez
	votre modification et poussez-la vers le serveur.

	Astuce 1~: vérifiez sur le serveur que votre branche a bien été
	créée et contient votre commit.

	Astuce 2~: en utilisant \texttt{gitk -{}-all}, vous pouvez voir
	graphiquement toutes vos branches.

    \item Utilisez la commande \texttt{git diff master} pour afficher les
	différences par rapport à la branche \texttt{master}

    \item Pour revenir à la branche principale, utilisez \texttt{git
	checkout master}

    \item Si vous n'êtes pas à l'aise avec \texttt{git} et les branches,
	précipitez-vous sur le site de \texttt{git}
	(\url{https://git-scm.com}) et le livre en ligne
	\url{https://git-scm.com/book/en/v2}

\end {enumerate}

Pour toutes vos modifications, prenez bien soin de repartir de la
branche \texttt{master}, c'est-à-dire de la version originale de xv6,
et de créer une nouvelle branche pour isoler vos modifications.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\question

Démarrez xv6 sans débogueur~: tapez \texttt{make qemu}. Une fenêtre
\texttt{qemu} s'ouvre, qui émule l'écran et le clavier d'un PC
standard. Xv6 démarre un shell simplifié. Dans cette fenêtre, vous
pouvez utiliser la commande \texttt{ls} de xv6, simplifiée elle aussi~:
les 3 nombres associés à chaque fichier sont~:

\begin {itemize}
    \item le type du fichier (1 pour répertoire, 2 pour fichier
	régulier, 3 pour périphérique)
    \item le numéro d'inode du fichier
    \item la taille du fichier en octets
\end {itemize}

Toujours dans la fenêtre \texttt{qemu}, utilisez la commande \texttt{cat
README}. Vous constatez que xv6 suppose que le clavier matériel émulé
par \texttt{qemu} est un clavier QWERTY. Vous verrez ultérieurement
que la lecture des touches du clavier US est déjà bien complexe...

En revanche, la fenêtre terminal dans laquelle vous avez lancé
\texttt{make qemu} affiche les mêmes informations que l'écran émulé
par \texttt{qemu}. Vous pouvez également taper les commandes dans cette
fenêtre, que \texttt{qemu} considère comme un terminal connecté par
une liaison série, qui envoie donc les codes ASCII correspondant aux
caractères saisis. Ceci permet d'utiliser le clavier normal que votre
système natif reconnaît correctement. Xv6 diffère d'un système
standard en ceci qu'il attend des informations indifféremment du
matériel «~clavier~» ou du matériel «~contrôleur de périphérique
de liaison série~».

Vous pouvez utiliser \texttt{make qemu-nox} pour démarrer xv6
sans fenêtre parasite d'émulation du clavier et de l'écran.

Si vous avez un ordinateur portable, vous constaterez vraisemblablement
pendant cet exercice qu'il chauffe et que le ventilateur se met en
route. Vous pouvez utiliser \texttt{top} pour constater que \texttt{qemu}
consomme énormément de temps CPU.

Laissez votre processeur refroidir un peu en fermant la fenêtre
\texttt{qemu} ou en utilisant \texttt{kill} sur le processus
\texttt{qemu}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\question

Avant d'utiliser un débogueur pour interagir avec le noyau,
il faut ajouter la ligne suivante (cf cours) dans le fichier
\texttt{\$HOME/.gdbinit}~:

\begin {quote}
    \texttt{add-auto-load-safe-path }\emph{/le/chemin/où/vous/avez/placé}\texttt{/xv6-public/.gdbinit}
\end {quote}

Pour faciliter l'utilisation du débogueur, il est préférable de
modifier le fichier \texttt{Makefile} pour~:

\begin {itemize}
    \item changer le niveau d'optimisation \texttt{-O2} en \texttt{-Og}
	lors de la compilation (n'oubliez pas de forcer la recompilation
	de tous les fichiers en faisant \texttt{make clean}) ;

    \item émuler un seul processeur, en mettant la variable
	\texttt{CPUS} à 1.
\end {itemize}

Vous pouvez ensuite lancer \texttt{make qemu-gdb} ou \texttt{make
qemu-nox-gdb}. Dans une autre fenêtre terminal, depuis le même
répertoire, démarrez \texttt{gdb} (sans argument), puis tapez la
commande \texttt{c} (ou \texttt{continue}) pour que \texttt{gdb}
donne à \texttt{qemu} l'ordre de démarrer. Vous pouvez taper
\framebox{Ctrl}\framebox{C} dans \texttt{gdb} pour suspendre l'exécution
à tout moment et interagir avec \texttt{gdb} et le noyau xv6.

La table ci-après liste quelques commandes de \texttt{gdb} (cf
\url{https://www.gnu.org/software/gdb/documentation/})~:

\begin {center}
\small
\begin {tabular} {lll} % \hline
    Commande & Raccourci & Explication \\ \hline
    \texttt{window} & \texttt{win} & scinde la fenêtre en deux (source en haut
	/ commandes gdb en bas)
	\\
    \texttt{break mafct} & \texttt{b} & pose un point d'arrêt
	à l'entrée de \texttt{mafct}
	\\
    \texttt{continue} & \texttt{c} & lance l'exécution
	jusqu'au prochain point d'arrêt
	\\
    \texttt{advance 123} & & continue l'exécution
	jusqu'à la ligne 123
	\\
    \texttt{finish} & \texttt{fin} & termine l'exécution de la fonction courante
	\\
    \texttt{where} & \texttt{bt} & affiche la pile
	\\
    \texttt{print *toto[10]} & \texttt{p} & affiche l'élément
	pointé par le 11e élément du
	tableau \texttt{toto}
	\\
    \texttt{print /x *toto[10]} & \texttt{p} & idem, mais affichage en hexadécimal
	\\
    \texttt{step} & \texttt{s} & exécute une
	instruction C
	\\
    \texttt{next} & \texttt{n} & exécute une
	instruction C sans s'arrêter si appel de fonction
	\\
    \texttt{stepi} & \texttt{si} & exécute une instruction assembleur
	\\
    \texttt{info registers} & \texttt{i r} & affiche le contenu
	des registres du processeur
	\\
    \texttt{x/10wd toto} & \texttt{x} &
	affiche 10 mots (\textit{\textbf{w}ord\/},
	32 bits) en \textbf{d}écimal à partir de l'adresse
	\texttt{toto}
	\\
    \texttt{kill} & \texttt{k} & termine xv6
	\\
    \texttt{quit} & \texttt{q} & quitte le débogueur
	\\
    % \hline
\end{tabular}
\end{center}

Pour vous familiariser avec \texttt{gdb}, placez un point d'arrêt dans
la fonction \texttt{sys\_chdir}, puis lancez l'exécution. Tapez la
commande «~\texttt{cd~.}~». Lorsque \texttt{gdb} s'arrête, avancez
juste après l'initialisation de la variable \texttt{curproc}, puis
affichez le contenu du descripteur de processus~: quels sont le numéro
et l'état du processus ? Quelle est l'adresse de la pile noyau du
processus~? Quel est le numéro du processus père~?

Lors de l'appel d'une primitive système, le noyau sauvegarde les
registres du processeur manipulés par le processus dans la structure
\texttt{trapframe} (voir \texttt{x86.h}), elle-même située dans le
descripteur de processus (champ \texttt{tf}, voir \texttt{proc.h}). Quelle
est l'adresse (en hexadécimal) de la prochaine instruction que le
processus exécutera ? Quelle est l'adresse de la pile utilisateur~?

Pourquoi les adresses des deux piles (utilisateur et noyau) sont-elles
si différentes ?

Avancez jusqu'à l'appel de \texttt{ilock}. Quelle est la valeur de
l'argument de la primitive \texttt{chdir}, placé dans la variable
\texttt{path} par la fonction \texttt{argstr}~?

Avant de quitter la session \texttt{gdb} avec la commande \texttt{q},
n'oubliez pas d'arrêter le programme en cours avec \texttt{k}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\question

Réalisez votre première modification dans le noyau de xv6~:
dans le fichier \texttt{syscall.c}, fonction \texttt{syscall}, juste
après la ligne «~\texttt{num = curproc->tf->eax}~», affichez la
valeur de \texttt{num} (numéro de la primitive système appelée)
avec la fonction \texttt{cprintf} qui s'utilise à peu près comme la
fonction \texttt{printf} de la bibliothèque standard.

Recompilez xv6 et démarrez-le (sans débogueur, c'est plus
simple). Contemplez le résultat de votre modification. Vous pouvez
faire \texttt{git checkout -f syscall.c} pour restaurer le fichier dans
son état d'origine (il n'est sans doute pas très utile de sauvegarder
cette modification).

Pourquoi faut-il utiliser dans le noyau xv6 une fonction \texttt{cprintf}
et non la vraie fonction \texttt{printf}~?

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\question

Au démarrage de l'ordinateur, le BIOS est le programme chargé de
lire le noyau depuis le disque dur et de le placer en mémoire à
partir de l'adresse 0x10\,0000. Une fois le noyau en mémoire, le BIOS
lui transfère l'exécution à partir de l'étiquette \texttt{entry}
(fichier \texttt{entry.S}).

Une des premières missions du code de xv6 à cette adresse est d'activer
la traduction mémoire via l'unité de gestion mémoire (MMU). Pour
ce faire, xv6 utilise le tableau constant \texttt{entrypgdir} (fichier
\texttt{main.c}) comme table des pages initiale et temporaire, avec la
caractéristique particulière qu'il s'agit d'une table simple de 1024
pages de 4~Mo (et non de 4~Ko), telle que décrite dans les figures~4.3
et 4.4 du «~\emph{Intel 64 and IA-32 Architectures Software Developer’s
Manual}~» \url{https://software.intel.com/en-us/articles/intel-sdm}.

En vous aidant du source (fichier \texttt{main.c} et fichiers inclus comme
\texttt{mmu.h}) et des figures ci-dessus mentionnées, donnez le contenu
de cette table (indices, contenu précis de chaque entrée). Quelle est
la signification du contenu (les bits) de chaque entrée ?

Une fois l'adresse de cette table configurée dans la MMU (instruction
«~\texttt{movl~\%eax,\%cr3}~», fichier \texttt{entry.S}) comment le
noyau perçoit-il la mémoire ? Représentez graphiquement (avec les adresses
numériques) les plages d'adresses accessibles par le noyau et les
adresses virtuelles et physiques correspondantes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\question

Xv6 dispose d'un nombre limité de commandes. Le but de l'exercice est
d'ajouter la commande \texttt{mv}. Vous vous limiterez ici à la forme
simple permettant de renommer un fichier~:

\begin {quote}
    \texttt{mv \emph{nom-actuel} \emph{nouveau-nom}}
\end {quote}

Rédigez cette commande, en prenant soin de vérifier le nombre
d'arguments. Terminez l'exécution en appelant la primitive \texttt{exit},
sans argument dans xv6, sinon vous aurez un message d'erreur. On rappelle
que le renommage d'un fichier est une succession d'opérations sur les
liens physiques, grâce aux primitives \texttt{link} et \texttt{unlink}.

Où faut-il modifier le fichier \texttt{Makefile} pour ajouter votre
commande~? Vous noterez une particularité dans le nom de l'exécutable
pour votre commande~: pouvez-vous en trouver la raison ?
