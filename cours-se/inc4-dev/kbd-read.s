// attendre que l'output buffer reçoive une donnée du clavier

attente:
    IN AL,0x64	  // AL = status register
    TEST AL,0x01  // teste le bit$_0$ du registre AL
    JZ attente	  // bit$_0 = 0 \Rightarrow$ rien dans l'output buffer

// l'output buffer contient quelque chose : le lire

    IN AL, 0x60   // AL = code de la touche
