void verrouiller (char *verrou) {
  int fd ;

  do {
    fd = open (verrou, O_CREAT | O_WRONLY | O_EXCL, 0666) ;
    if (fd == -1 && errno == EEXIST)
      sleep (1) ;        // attente active ralentie
  } while (fd == -1 && errno == EEXIST) ;
  if (fd == -1)
    raler (...) ;
  close (fd) ;
}

void deverrouiller (char *verrou) {
  unlink (verrou) ;
}
