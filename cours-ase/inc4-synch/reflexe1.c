// avant
compte = compte + 10 ;	// variable partagée par plusieurs threads

// après
pthread_mutex_lock (&verrou) ;	// verrou doit être également partagée
compte = compte + 10 ;	// accès en exclusion mutuelle
pthread_mutex_unlock (&verrou) ;
