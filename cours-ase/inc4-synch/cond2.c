condition c ;
mutex m ;	// pour les sections critiques
file *f ;

void *thread_traite (void *arg) {
  lock (&m) ;
  if (file_vide(f)) {
    cwait (&c) ;
  }
  unlock (&m) ;
  traiter (extraire_file (f)) ;
}

void *thread_produit (void *arg) {
  d = lire_donnee (...) ;
  lock (&m) ; // si besoin de section critique
  ajouter_file (f, d) ;
  csignal (&c) ;
  unlock (&m) ;
}
