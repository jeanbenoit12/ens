struct {
  struct proc proc [NPROC] ;	// table des processus
  int verrou ;			// verrou pour la table
} tproc ;			// table utilisable en concurrence

struct proc *chercher_proc_libre (void) {
  verrouiller (&tproc.verrou) ;
  for (int i = 0 ; i < NPROC ; i++) {
    if (tproc.proc [i].etat == LIBRE) {	// si deux processeurs arrivent sur la même entrée simultanément
      tproc.proc [i].etat = CREATION ;	// ils l'alloueraient deux fois \implique section critique nécessaire
      deverrouiller (&tproc.verrou) ;
      return &tproc.proc [i] ;
    }
  }
  deverrouiller (&tproc.verrou) ;
  return NULL ;
}
