P (sem) {
    sem.compteur-- ;		// consommation d'une ressource
    if (sem.compteur < 0)	// plus de ressource disponible
	attendre () ;		// mettre l'activité en attente
}

V (sem) {
    sem.compteur++ ;		// libération d'une ressource
    if (sem.compteur <= 0)
	reveiller () ;		// réveiller une des activités en attente
}
