// ne pas faire : tout est déstructuré
int compteur ;
pthread_mutex_t m ;

void *fct_thread (void *arg) {
    ...
    pthread_mutex_lock (&m) ;
    compteur += y ;
    pthread_mutex_unlock (&m) ;
    ...
}

// mieux : le compteur x est un objet contenant son propre
// verrou, les accès se font via des fonctions (méthodes)
// atomiques
struct compteur {
    int compteur ;
    pthread_mutex_t m ;
} x ;

void ajouter (struct compteur *c, int y) {
    pthread_mutex_lock (&c->m) ;
    c->compteur += y ;
    pthread_mutex_unlock (&c->m) ;
}

void *fct_thread (void *arg) {
    ...
    ajouter (&x, y) ;
    ...
}
