> lspci -tv			# chercher le périphérique qui nous intéresse
...
      +-1c.2-[02]----00.0  Qualcomm Atheros Wireless Network Adapter

> lspci -tn			# obtenir son identification (vendor id, dev id)
...
      +-1c.2-[02]----00.0  168c:003e

> lspci -v -d 168c:003e		# ou bien -s 02:00.0
02:00.0 Network controller: Qualcomm Atheros Wireless Network Adapter (rev 32)
    Subsystem: Rivet Networks Killer 1435 Wireless-AC
    Flags: bus master, fast devsel, latency 0, IRQ 144
    Memory at dc000000 (64-bit, non-prefetchable) [size=2M]
    Capabilities: [40] Power Management version 3
    Capabilities: [50] MSI: Enable+ Count=1/8 Maskable+ 64bit-
    ...
    Kernel driver in use: ath10k_pci
    Kernel modules: ath10k_pci
