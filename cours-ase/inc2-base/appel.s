f:  sub  1,%sp		// laisser de la place pour y

    ld   [%sp+2],%a	// a $\leftarrow$ argument x
    add  2,%a		// a $\leftarrow$ a+2
    push %a		// empiler x+2 (v)
    push 10		// empiler 10 (u)
    call g
    add  2,%sp		// dépiler les 2 arguments
    st   %a,[%sp]	// y $\leftarrow$ résultat de g (dans a)

    ld   [%sp],%a	// a $\leftarrow$ y (pourrait être optimisé)
    add  20,%a		// a $\leftarrow$ y+20

    add  1,%sp		// fin d'existence pour y
    rtn			// résultat dans a

g:  ld   [%sp+1],%a	// a $\leftarrow$ u
    add  1,%a           // a $\leftarrow$ u+1
    push %b		// on va utiliser b, on le sauvegarde
    ld   [%sp+3],%b	// b $\leftarrow$ v
    add  2,%b           // b $\leftarrow$ v+2
    mul  %b,%a		// a $\leftarrow$ a*b = (u+1)*(v+2)
    pop  %b		// restaurer b
    rtn			// résultat dans a
