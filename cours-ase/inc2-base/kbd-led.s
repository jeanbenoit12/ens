#define KBD_REG_CTRL		KBD_REG_STATUS	// même numéro de port
#define	KBD_CTRL_LED		0x01		// codes extraits de la doc
#define	KBD_DATA_LED_ON		0x08		// allumer une LED du clavier
#define	KBD_LED_NUMLOCK		1		// numéros des LED du clavier
#define	KBD_LED_CAPSLOCK	2

#define	CAPS_ON	  KBD_DATA_LED_ON|KBD_LED_CAPSLOCK // valeur calculée par l'assembleur

// allumer la LED "Caps Lock"
allumer_caps_lock:
	out CAPS_ON,[KBD_REG_DATA]		// numéro de la LED + allumer
	out KBD_CTRL_LED,[KBD_REG_CTRL]		// envoyer l'ordre d'allumer la LED
	rtn
