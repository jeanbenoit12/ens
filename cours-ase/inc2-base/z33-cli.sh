# \texttt{run} : exécute le fichier \texttt{fact.s} à partir de l'étiquette \texttt{main} jusqu'à l'instruction \texttt{reset}
> ./z33-cli run fact.s main
Reading program path="fact.S"
Executing instruction "push 5"
...
Executing instruction "reset"
End of program registers=%a = 120 | %b = 0 | %pc = 103 | %sp = 9999 | %sr = ZERO

# \texttt{run} avec l'option \texttt{-i} : démarre l'exécution en mode \frquote{interactif}
> ./z33-cli run -i fact.s main
Running in interactive mode. Type "help" to list available commands.
>> registers			# affiche le contenu des registres
Registers: %a = 0 | %b = 0 | %pc = 100 | %sp = 10000 | %sr = (empty)
>> step				# exécute l'instruction suivante
Executing instruction "push 5"
>> break factorielle		# pose un point d'arrêt à l'adresse \frquote{factorielle} (ici à l'adresse 200)
Setting a breakpoint address=200
>> continue			# lance l'exécution jusqu'à l'instruction \texttt{reset} ou un point d'arrêt
Stopped at a breakpoint address=200
>> registers
...
