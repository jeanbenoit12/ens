SUBDIRS = \
	conseils \
	cours-ase \
	cours-cse \
	cours-diu-eil \
	cours-ps \
	cours-sc \
	cours-se \
	cours-sh \
	cours-reseau \
	exercices \
	exv6 \
	manuel \
	mem-stage \
	mem-ter \
	z33refcard \

.PHONY: all $(SUBDIRS)

all: $(SUBDIRS)

conseils:
	cd conseils ; make

cours-ase:
	cd cours-ase ; make all tout.pdf print-tout.pdf

cours-cse:
	cd cours-cse ; make all tout.pdf print-tout.pdf

cours-diu-eil:
	cd cours-diu-eil ; make tout.pdf print-tout.pdf

cours-ps:
	cd cours-ps ; make all tout.pdf print-tout.pdf print-tout765.pdf

cours-sc:
	cd cours-sc ; make tout.pdf print-tout.pdf

cours-se:
	cd cours-se ; make tout.pdf print-tout.pdf

cours-sh:
	cd cours-sh ; make tout.pdf print-tout.pdf

cours-reseau:
	cd cours-reseau ; make all tout.pdf print-tout.pdf

exercices:
	cd exercices ; make

exv6:
	cd exv6 ; make

manuel:
	cd manuel ; make

mem-stage:
	cd mem-stage ; make

mem-ter:
	cd mem-ter ; make

z33refcard:
	cd z33refcard ; make

index.html: README.md
	pandoc \
	    --standalone \
	    --metadata="pagetitle:Supports d'enseignement de Pierre David" \
	    --variable='lang:fr' \
	    --variable='author-meta:Pierre David' \
	    --variable='keywords:operating system' \
	    --variable='keywords:unix' \
	    --variable='highlighting-css: *{font-family:Arial,sans-serif;}' \
	    -o index.html \
	    README.md

clean:
	for i in $(SUBDIRS) ; do (cd $$i ; make clean) ; done
	rm -f index.html
